package net.asam.openscenario.osc2.tests;

import com.google.inject.Inject;
import net.asam.openscenario.osc2.osc2.OscFile;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(OSC2InjectorProvider.class)
@SuppressWarnings("all")
public class Osc2StructTest {
  @Inject
  private ParseHelper<OscFile> parseHelper;

  @Test
  public void loadSimpleStruct() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("struct Structure");
      _builder.newLine();
      final OscFile result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Unexpected errors: �errors.join(\", \")�");
      Assertions.assertTrue(_isEmpty, _builder_1.toString());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  @Test
  public void loadStructWithErrors() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("struct Structure:");
      _builder.newLine();
      _builder.append("struct Struct2:");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("p1: int");
      _builder.newLine();
      final OscFile result = this.parseHelper.parse(_builder);
      Assertions.assertNotNull(result);
      final EList<Resource.Diagnostic> errors = result.eResource().getErrors();
      boolean _isEmpty = errors.isEmpty();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("Expected errors: �errors.join(\", \")�");
      Assertions.assertFalse(_isEmpty, _builder_1.toString());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
