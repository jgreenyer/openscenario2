package net.asam.openscenario.osc2.tests;

import java.util.List;
import javax.inject.Inject;
import net.asam.openscenario.osc2.osc2.OscFile;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.validation.Issue;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(OSC2InjectorProvider.class)
@SuppressWarnings("all")
public class TestBase {
  @Inject
  protected ParseHelper<OscFile> parseHelper;

  @Inject
  protected ValidationTestHelper validator;

  public void assertValid(final String s) {
    try {
      final OscFile result = this.parseHelper.parse(s);
      Assertions.assertNotNull(result);
      this.validator.assertNoErrors(result);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  public void assertError(final String s, final String error) {
    try {
      final OscFile result = this.parseHelper.parse(s);
      Assertions.assertNotNull(result);
      this.containsError(result, error);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  public void containsError(final EObject eObject, final String stringContainedInErrorMessage) {
    final List<Issue> allIssues = this.validator.validate(eObject);
    for (final Issue issue : allIssues) {
      {
        InputOutput.<Issue>println(issue);
        boolean _contains = issue.getMessage().contains(stringContainedInErrorMessage);
        if (_contains) {
          return;
        }
      }
    }
    throw new AssertionError((("No issue containing the substring \'" + stringContainedInErrorMessage) + "\' occurred"));
  }
}
