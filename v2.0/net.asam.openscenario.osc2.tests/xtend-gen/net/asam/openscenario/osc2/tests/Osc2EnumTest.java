package net.asam.openscenario.osc2.tests;

import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(OSC2InjectorProvider.class)
@SuppressWarnings("all")
public class Osc2EnumTest extends TestBase {
  @Test
  public void loadEnumOnlyModel() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Vehicle: [car=0, truck=1, bus=2]");
    _builder.newLine();
    _builder.append("enum direction: [north, west, east, south]");
    _builder.newLine();
    this.assertValid(_builder.toString());
  }

  @Test
  public void enumMemberValueAlreadyDefined() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Vehicle: [car=0, truck=1, bus=1]");
    _builder.newLine();
    this.assertError(_builder.toString(), "Value already defined");
  }

  @Test
  public void EnumTypeExtensionValid() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Speed:[slow=0, medium=1, fast=2]");
    _builder.newLine();
    _builder.append("extend Speed: [very_slow]");
    _builder.newLine();
    _builder.append("extend Speed: [very_fast=3]");
    _builder.newLine();
    this.assertValid(_builder.toString());
  }

  @Test
  public void EnumTypeExtensionDuplicateName() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Speed:[slow=0, medium=1, fast=2]");
    _builder.newLine();
    _builder.append("extend Speed: [very_slow]");
    _builder.newLine();
    _builder.append("extend Speed: [very_fast, slow]");
    _builder.newLine();
    this.assertError(_builder.toString(), "Value name already defined");
  }

  @Test
  public void EnumTypeExtensionDuplicateValue() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Speed:[slow=0, medium=1, fast=2]");
    _builder.newLine();
    _builder.append("extend Speed: [very_slow]");
    _builder.newLine();
    _builder.append("extend Speed: [very_fast=2]");
    _builder.newLine();
    this.assertError(_builder.toString(), "Value already defined");
  }

  @Test
  public void EnumTypeExtensionDuplicateValueInExtensions() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Speed:[slow=0, medium=1, fast=2]");
    _builder.newLine();
    _builder.newLine();
    _builder.append("extend Speed: [very_slow=3]");
    _builder.newLine();
    _builder.append("extend Speed: [very_fast=3]");
    _builder.newLine();
    this.assertError(_builder.toString(), "Value already defined");
  }

  @Test
  public void differentEnumsAndReferences() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum WindSpeed: [NoWind, Calm, LightAir, LightBreeze, GentleBreeze, ModerateBreeze, FreshBreeze, StrongBreeze, NearGale, Gale, StrongGale, Storm, ViolentStorm, Hurricane]");
    _builder.newLine();
    _builder.newLine();
    _builder.append("enum Speed:[slow=0, medium=1, high=2]");
    _builder.newLine();
    _builder.newLine();
    _builder.append("struct Environment:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("windSpeed1 : WindSpeed = LightAir");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("windSpeed2 : WindSpeed = WindSpeed!Storm");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("vehicleSpeed1 : Speed = slow");
    _builder.newLine();
    this.assertValid(_builder.toString());
  }
}
