package net.asam.openscenario.osc2.tests;

import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(OSC2InjectorProvider.class)
@SuppressWarnings("all")
public class With_Keep_it extends TestBase {
  @Test
  public void with_keep_it() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum Color : [Red, Blue, Green]");
    _builder.newLine();
    _builder.append("extend Color : [Yellow, Black]");
    _builder.newLine();
    _builder.newLine();
    _builder.append("type Speed is SI(m: 1, s: -1)");
    _builder.newLine();
    _builder.append("unit kmph of Speed is SI(m: 1, s: -1, factor: 0.277777778)");
    _builder.newLine();
    _builder.newLine();
    _builder.append("actor Vehicle:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("speed : Speed");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("color : Color");
    _builder.newLine();
    _builder.newLine();
    _builder.append("struct System:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("vehicle : Vehicle with:");
    _builder.newLine();
    _builder.append("\t\t");
    _builder.append("keep (it.speed <= 100 kmph and it.color == Yellow)");
    _builder.newLine();
    this.assertValid(_builder.toString());
  }
}
