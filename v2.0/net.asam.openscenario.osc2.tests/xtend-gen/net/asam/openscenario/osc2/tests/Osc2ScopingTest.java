package net.asam.openscenario.osc2.tests;

import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(OSC2InjectorProvider.class)
@SuppressWarnings("all")
public class Osc2ScopingTest extends TestBase {
  @Test
  public void simpleScenario() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("enum WindSpeed: [NoWind, Calm, LightAir, LightBreeze, GentleBreeze, ModerateBreeze, FreshBreeze, StrongBreeze, NearGale, Gale, StrongGale, Storm, ViolentStorm, Hurricane]");
    _builder.newLine();
    _builder.newLine();
    _builder.append("struct Environment:");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("windSpeed1 : WindSpeed = LightAir");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("windSpeed2 : WindSpeed = WindSpeed!Storm");
    _builder.newLine();
    _builder.append("\t");
    _builder.append("windSpeed3 : WindSpeed = GentleBreeze");
    _builder.newLine();
    this.assertValid(_builder.toString());
  }
}
