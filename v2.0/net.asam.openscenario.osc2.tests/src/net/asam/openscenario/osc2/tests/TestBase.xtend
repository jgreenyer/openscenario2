package net.asam.openscenario.osc2.tests

import org.junit.jupiter.api.^extension.ExtendWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.InjectWith
import javax.inject.Inject
import org.eclipse.xtext.testing.util.ParseHelper
import net.asam.openscenario.osc2.osc2.OscFile
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import org.junit.jupiter.api.Assertions
import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.validation.Issue
import java.util.List

@ExtendWith(InjectionExtension)
@InjectWith(OSC2InjectorProvider)
class TestBase {
	
	@Inject
	protected ParseHelper<OscFile> parseHelper
	@Inject protected ValidationTestHelper validator	


	def void assertValid(String s){
		val result = parseHelper.parse(s)
		Assertions.assertNotNull(result)
		validator.assertNoErrors(result)
	}

	def void assertError(String s, String error){
		val result = parseHelper.parse(s)
		Assertions.assertNotNull(result)
		containsError(result, error);
	}
	
	def void containsError(EObject eObject, String stringContainedInErrorMessage){
		val List<Issue> allIssues = validator.validate(eObject);
		for (issue : allIssues) {
			println(issue)
			if(issue.message.contains(stringContainedInErrorMessage)) return
		}
		throw new AssertionError("No issue containing the substring '"+ stringContainedInErrorMessage + "' occurred")
	}
	
	
	
}