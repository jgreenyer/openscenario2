package net.asam.openscenario.osc2.tests

import com.google.inject.Inject
import net.asam.openscenario.osc2.osc2.OscFile
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.eclipse.xtext.testing.util.ParseHelper
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(OSC2InjectorProvider)
class Osc2StructTest {
	@Inject
	ParseHelper<OscFile> parseHelper
	
	@Test
	def void loadSimpleStruct() {
		val result = parseHelper.parse('''
			struct Structure
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertTrue(errors.isEmpty, '''Unexpected errors: �errors.join(", ")�''')
	}
	
	@Test
	def void loadStructWithErrors() {
		val result = parseHelper.parse('''
			struct Structure:
			struct Struct2:
				p1: int
		''')
		Assertions.assertNotNull(result)
		val errors = result.eResource.errors
		Assertions.assertFalse(errors.isEmpty, '''Expected errors: �errors.join(", ")�''')
	}
}