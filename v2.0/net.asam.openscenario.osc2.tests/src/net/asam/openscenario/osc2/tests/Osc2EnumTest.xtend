package net.asam.openscenario.osc2.tests

import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(OSC2InjectorProvider)
class Osc2EnumTest extends TestBase{	
	
	
	@Test
	def void loadEnumOnlyModel() {
		assertValid('''
			enum Vehicle: [car=0, truck=1, bus=2]
			enum direction: [north, west, east, south]
		''')
	}

	@Test
	def void enumMemberValueAlreadyDefined() {
		assertError('''
			enum Vehicle: [car=0, truck=1, bus=1]
		''', "Value already defined")
	}
	
	
	@Test
	def void EnumTypeExtensionValid() {
		assertValid('''
			enum Speed:[slow=0, medium=1, fast=2]
			extend Speed: [very_slow]
			extend Speed: [very_fast=3]
		''')
	}

	@Test
	def void EnumTypeExtensionDuplicateName() {
		assertError('''
			enum Speed:[slow=0, medium=1, fast=2]
			extend Speed: [very_slow]
			extend Speed: [very_fast, slow]
		''', "Value name already defined")
	}

	@Test
	def void EnumTypeExtensionDuplicateValue() {
		assertError('''
			enum Speed:[slow=0, medium=1, fast=2]
			extend Speed: [very_slow]
			extend Speed: [very_fast=2]
		''', "Value already defined")
	}

	@Test
	def void EnumTypeExtensionDuplicateValueInExtensions() {
		
		// TODO: implement value check across multiple enum type extensions 
		
		assertError('''
			enum Speed:[slow=0, medium=1, fast=2]
			
			extend Speed: [very_slow=3]
			extend Speed: [very_fast=3]
		''', "Value already defined")
	}




	
	
	@Test
	def void differentEnumsAndReferences() {
		assertValid('''
			enum WindSpeed: [NoWind, Calm, LightAir, LightBreeze, GentleBreeze, ModerateBreeze, FreshBreeze, StrongBreeze, NearGale, Gale, StrongGale, Storm, ViolentStorm, Hurricane]
			
			enum Speed:[slow=0, medium=1, high=2]
			
			struct Environment:
				windSpeed1 : WindSpeed = LightAir
				windSpeed2 : WindSpeed = WindSpeed!Storm
				vehicleSpeed1 : Speed = slow
		''')
	}
	
}