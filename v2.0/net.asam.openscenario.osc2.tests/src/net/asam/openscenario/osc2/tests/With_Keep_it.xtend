package net.asam.openscenario.osc2.tests

import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(OSC2InjectorProvider)
class With_Keep_it extends TestBase{
	
	@Test
	def void with_keep_it(){
		assertValid('''
			enum Color : [Red, Blue, Green]
			extend Color : [Yellow, Black]
			
			type Speed is SI(m: 1, s: -1)
			unit kmph of Speed is SI(m: 1, s: -1, factor: 0.277777778)
			
			actor Vehicle:
				speed : Speed
				color : Color
			
			struct System:
				vehicle : Vehicle with:
					keep (it.speed <= 100 kmph and it.color == Yellow)
		''')
	}
	
}