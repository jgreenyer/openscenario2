package net.asam.openscenario.osc2.tests

import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.^extension.ExtendWith

@ExtendWith(InjectionExtension)
@InjectWith(OSC2InjectorProvider)
class Osc2ScopingTest extends TestBase{
	

	@Test
	def void simpleScenario() {
		assertValid('''
			enum WindSpeed: [NoWind, Calm, LightAir, LightBreeze, GentleBreeze, ModerateBreeze, FreshBreeze, StrongBreeze, NearGale, Gale, StrongGale, Storm, ViolentStorm, Hurricane]
			
			struct Environment:
				windSpeed1 : WindSpeed = LightAir
				windSpeed2 : WindSpeed = WindSpeed!Storm
				windSpeed3 : WindSpeed = GentleBreeze
		''')
	}
	
	
}


	