/*
 * generated by Xtext 2.25.0
 */
package net.asam.openscenario.osc2;


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
public class OSC2StandaloneSetup extends OSC2StandaloneSetupGenerated {

	public static void doSetup() {
		new OSC2StandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}
