package net.asam.openscenario.osc2.parser.antlr.lexer;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalOSC2Lexer extends Lexer {
    public static final int Enum=37;
    public static final int Import=18;
    public static final int Or=75;
    public static final int EqualsSignGreaterThanSign=62;
    public static final int Var=56;
    public static final int String=24;
    public static final int False=30;
    public static final int LessThanSign=87;
    public static final int LeftParenthesis=78;
    public static final int Bool=34;
    public static final int Actor=26;
    public static final int ExclamationMark=76;
    public static final int GreaterThanSign=89;
    public static final int Hard=39;
    public static final int RULE_DIGIT=102;
    public static final int Scenario=11;
    public static final int Offset=19;
    public static final int GreaterThanSignEqualsSign=63;
    public static final int Float=31;
    public static final int EqualsSignEqualsSign=61;
    public static final int PlusSign=81;
    public static final int LeftSquareBracket=94;
    public static final int If=68;
    public static final int A=92;
    public static final int In=69;
    public static final int Is=70;
    public static final int It=71;
    public static final int K=93;
    public static final int M=97;
    public static final int Comma=82;
    public static final int Uint=46;
    public static final int As=65;
    public static final int HyphenMinus=83;
    public static final int S=98;
    public static final int SI=64;
    public static final int LessThanSignEqualsSign=60;
    public static final int Solidus=85;
    public static final int Call=35;
    public static final int FullStop=84;
    public static final int Default=12;
    public static final int CommercialAt=91;
    public static final int KW__=96;
    public static final int Serial=23;
    public static final int Type=45;
    public static final int Cover=27;
    public static final int Expression=5;
    public static final int QuestionMark=90;
    public static final int Event=28;
    public static final int RULE_HEX_DIGIT=104;
    public static final int RULE_FLOAT=107;
    public static final int ExclamationMarkEqualsSign=57;
    public static final int One_of=20;
    public static final int HyphenMinusGreaterThanSign=58;
    public static final int Kg=72;
    public static final int Cd=66;
    public static final int RULE_BEGIN=99;
    public static final int Keep=40;
    public static final int True=44;
    public static final int Parallel=10;
    public static final int Unit=47;
    public static final int Global=17;
    public static final int PercentSign=77;
    public static final int RULE_UINT=103;
    public static final int FullStopFullStop=59;
    public static final int Factor=16;
    public static final int List=41;
    public static final int RightSquareBracket=95;
    public static final int Remove_default=4;
    public static final int Undefined=6;
    public static final int RULE_NEG_INT=106;
    public static final int Inherits=8;
    public static final int RightParenthesis=79;
    public static final int Do=67;
    public static final int Range=32;
    public static final int Not=54;
    public static final int Extend=15;
    public static final int And=50;
    public static final int External=7;
    public static final int Elapsed=13;
    public static final int Fall=38;
    public static final int Wait=48;
    public static final int Action=14;
    public static final int RULE_END=100;
    public static final int RULE_IDENTIFIER=101;
    public static final int RULE_STRING=108;
    public static final int Int=52;
    public static final int Struct=25;
    public static final int Rise=43;
    public static final int With=49;
    public static final int RULE_SL_COMMENT=109;
    public static final int EqualsSign=88;
    public static final int Emit=36;
    public static final int Record=21;
    public static final int Colon=86;
    public static final int EOF=-1;
    public static final int Modifier=9;
    public static final int Asterisk=80;
    public static final int Until=33;
    public static final int Def=51;
    public static final int RULE_WS=110;
    public static final int Sample=22;
    public static final int Only=42;
    public static final int Mol=53;
    public static final int RULE_ANY_OTHER=111;
    public static final int Rad=55;
    public static final int Of=73;
    public static final int Every=29;
    public static final int RULE_HEX_UINT=105;
    public static final int On=74;

    // delegates
    // delegators

    public InternalOSC2Lexer() {;} 
    public InternalOSC2Lexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalOSC2Lexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalOSC2Lexer.g"; }

    // $ANTLR start "Remove_default"
    public final void mRemove_default() throws RecognitionException {
        try {
            int _type = Remove_default;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:14:16: ( 'remove_default' )
            // InternalOSC2Lexer.g:14:18: 'remove_default'
            {
            match("remove_default"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Remove_default"

    // $ANTLR start "Expression"
    public final void mExpression() throws RecognitionException {
        try {
            int _type = Expression;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:16:12: ( 'expression' )
            // InternalOSC2Lexer.g:16:14: 'expression'
            {
            match("expression"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Expression"

    // $ANTLR start "Undefined"
    public final void mUndefined() throws RecognitionException {
        try {
            int _type = Undefined;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:18:11: ( 'undefined' )
            // InternalOSC2Lexer.g:18:13: 'undefined'
            {
            match("undefined"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Undefined"

    // $ANTLR start "External"
    public final void mExternal() throws RecognitionException {
        try {
            int _type = External;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:20:10: ( 'external' )
            // InternalOSC2Lexer.g:20:12: 'external'
            {
            match("external"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "External"

    // $ANTLR start "Inherits"
    public final void mInherits() throws RecognitionException {
        try {
            int _type = Inherits;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:22:10: ( 'inherits' )
            // InternalOSC2Lexer.g:22:12: 'inherits'
            {
            match("inherits"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Inherits"

    // $ANTLR start "Modifier"
    public final void mModifier() throws RecognitionException {
        try {
            int _type = Modifier;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:24:10: ( 'modifier' )
            // InternalOSC2Lexer.g:24:12: 'modifier'
            {
            match("modifier"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Modifier"

    // $ANTLR start "Parallel"
    public final void mParallel() throws RecognitionException {
        try {
            int _type = Parallel;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:26:10: ( 'parallel' )
            // InternalOSC2Lexer.g:26:12: 'parallel'
            {
            match("parallel"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Parallel"

    // $ANTLR start "Scenario"
    public final void mScenario() throws RecognitionException {
        try {
            int _type = Scenario;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:28:10: ( 'scenario' )
            // InternalOSC2Lexer.g:28:12: 'scenario'
            {
            match("scenario"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Scenario"

    // $ANTLR start "Default"
    public final void mDefault() throws RecognitionException {
        try {
            int _type = Default;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:30:9: ( 'default' )
            // InternalOSC2Lexer.g:30:11: 'default'
            {
            match("default"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Default"

    // $ANTLR start "Elapsed"
    public final void mElapsed() throws RecognitionException {
        try {
            int _type = Elapsed;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:32:9: ( 'elapsed' )
            // InternalOSC2Lexer.g:32:11: 'elapsed'
            {
            match("elapsed"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Elapsed"

    // $ANTLR start "Action"
    public final void mAction() throws RecognitionException {
        try {
            int _type = Action;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:34:8: ( 'action' )
            // InternalOSC2Lexer.g:34:10: 'action'
            {
            match("action"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Action"

    // $ANTLR start "Extend"
    public final void mExtend() throws RecognitionException {
        try {
            int _type = Extend;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:36:8: ( 'extend' )
            // InternalOSC2Lexer.g:36:10: 'extend'
            {
            match("extend"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Extend"

    // $ANTLR start "Factor"
    public final void mFactor() throws RecognitionException {
        try {
            int _type = Factor;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:38:8: ( 'factor' )
            // InternalOSC2Lexer.g:38:10: 'factor'
            {
            match("factor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Factor"

    // $ANTLR start "Global"
    public final void mGlobal() throws RecognitionException {
        try {
            int _type = Global;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:40:8: ( 'global' )
            // InternalOSC2Lexer.g:40:10: 'global'
            {
            match("global"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Global"

    // $ANTLR start "Import"
    public final void mImport() throws RecognitionException {
        try {
            int _type = Import;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:42:8: ( 'import' )
            // InternalOSC2Lexer.g:42:10: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Import"

    // $ANTLR start "Offset"
    public final void mOffset() throws RecognitionException {
        try {
            int _type = Offset;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:44:8: ( 'offset' )
            // InternalOSC2Lexer.g:44:10: 'offset'
            {
            match("offset"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Offset"

    // $ANTLR start "One_of"
    public final void mOne_of() throws RecognitionException {
        try {
            int _type = One_of;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:46:8: ( 'one_of' )
            // InternalOSC2Lexer.g:46:10: 'one_of'
            {
            match("one_of"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "One_of"

    // $ANTLR start "Record"
    public final void mRecord() throws RecognitionException {
        try {
            int _type = Record;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:48:8: ( 'record' )
            // InternalOSC2Lexer.g:48:10: 'record'
            {
            match("record"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Record"

    // $ANTLR start "Sample"
    public final void mSample() throws RecognitionException {
        try {
            int _type = Sample;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:50:8: ( 'sample' )
            // InternalOSC2Lexer.g:50:10: 'sample'
            {
            match("sample"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Sample"

    // $ANTLR start "Serial"
    public final void mSerial() throws RecognitionException {
        try {
            int _type = Serial;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:52:8: ( 'serial' )
            // InternalOSC2Lexer.g:52:10: 'serial'
            {
            match("serial"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Serial"

    // $ANTLR start "String"
    public final void mString() throws RecognitionException {
        try {
            int _type = String;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:54:8: ( 'string' )
            // InternalOSC2Lexer.g:54:10: 'string'
            {
            match("string"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "String"

    // $ANTLR start "Struct"
    public final void mStruct() throws RecognitionException {
        try {
            int _type = Struct;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:56:8: ( 'struct' )
            // InternalOSC2Lexer.g:56:10: 'struct'
            {
            match("struct"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Struct"

    // $ANTLR start "Actor"
    public final void mActor() throws RecognitionException {
        try {
            int _type = Actor;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:58:7: ( 'actor' )
            // InternalOSC2Lexer.g:58:9: 'actor'
            {
            match("actor"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Actor"

    // $ANTLR start "Cover"
    public final void mCover() throws RecognitionException {
        try {
            int _type = Cover;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:60:7: ( 'cover' )
            // InternalOSC2Lexer.g:60:9: 'cover'
            {
            match("cover"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Cover"

    // $ANTLR start "Event"
    public final void mEvent() throws RecognitionException {
        try {
            int _type = Event;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:62:7: ( 'event' )
            // InternalOSC2Lexer.g:62:9: 'event'
            {
            match("event"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Event"

    // $ANTLR start "Every"
    public final void mEvery() throws RecognitionException {
        try {
            int _type = Every;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:64:7: ( 'every' )
            // InternalOSC2Lexer.g:64:9: 'every'
            {
            match("every"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Every"

    // $ANTLR start "False"
    public final void mFalse() throws RecognitionException {
        try {
            int _type = False;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:66:7: ( 'false' )
            // InternalOSC2Lexer.g:66:9: 'false'
            {
            match("false"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "False"

    // $ANTLR start "Float"
    public final void mFloat() throws RecognitionException {
        try {
            int _type = Float;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:68:7: ( 'float' )
            // InternalOSC2Lexer.g:68:9: 'float'
            {
            match("float"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Float"

    // $ANTLR start "Range"
    public final void mRange() throws RecognitionException {
        try {
            int _type = Range;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:70:7: ( 'range' )
            // InternalOSC2Lexer.g:70:9: 'range'
            {
            match("range"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Range"

    // $ANTLR start "Until"
    public final void mUntil() throws RecognitionException {
        try {
            int _type = Until;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:72:7: ( 'until' )
            // InternalOSC2Lexer.g:72:9: 'until'
            {
            match("until"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Until"

    // $ANTLR start "Bool"
    public final void mBool() throws RecognitionException {
        try {
            int _type = Bool;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:74:6: ( 'bool' )
            // InternalOSC2Lexer.g:74:8: 'bool'
            {
            match("bool"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Bool"

    // $ANTLR start "Call"
    public final void mCall() throws RecognitionException {
        try {
            int _type = Call;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:76:6: ( 'call' )
            // InternalOSC2Lexer.g:76:8: 'call'
            {
            match("call"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Call"

    // $ANTLR start "Emit"
    public final void mEmit() throws RecognitionException {
        try {
            int _type = Emit;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:78:6: ( 'emit' )
            // InternalOSC2Lexer.g:78:8: 'emit'
            {
            match("emit"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Emit"

    // $ANTLR start "Enum"
    public final void mEnum() throws RecognitionException {
        try {
            int _type = Enum;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:80:6: ( 'enum' )
            // InternalOSC2Lexer.g:80:8: 'enum'
            {
            match("enum"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Enum"

    // $ANTLR start "Fall"
    public final void mFall() throws RecognitionException {
        try {
            int _type = Fall;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:82:6: ( 'fall' )
            // InternalOSC2Lexer.g:82:8: 'fall'
            {
            match("fall"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Fall"

    // $ANTLR start "Hard"
    public final void mHard() throws RecognitionException {
        try {
            int _type = Hard;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:84:6: ( 'hard' )
            // InternalOSC2Lexer.g:84:8: 'hard'
            {
            match("hard"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Hard"

    // $ANTLR start "Keep"
    public final void mKeep() throws RecognitionException {
        try {
            int _type = Keep;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:86:6: ( 'keep' )
            // InternalOSC2Lexer.g:86:8: 'keep'
            {
            match("keep"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Keep"

    // $ANTLR start "List"
    public final void mList() throws RecognitionException {
        try {
            int _type = List;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:88:6: ( 'list' )
            // InternalOSC2Lexer.g:88:8: 'list'
            {
            match("list"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "List"

    // $ANTLR start "Only"
    public final void mOnly() throws RecognitionException {
        try {
            int _type = Only;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:90:6: ( 'only' )
            // InternalOSC2Lexer.g:90:8: 'only'
            {
            match("only"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Only"

    // $ANTLR start "Rise"
    public final void mRise() throws RecognitionException {
        try {
            int _type = Rise;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:92:6: ( 'rise' )
            // InternalOSC2Lexer.g:92:8: 'rise'
            {
            match("rise"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Rise"

    // $ANTLR start "True"
    public final void mTrue() throws RecognitionException {
        try {
            int _type = True;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:94:6: ( 'true' )
            // InternalOSC2Lexer.g:94:8: 'true'
            {
            match("true"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "True"

    // $ANTLR start "Type"
    public final void mType() throws RecognitionException {
        try {
            int _type = Type;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:96:6: ( 'type' )
            // InternalOSC2Lexer.g:96:8: 'type'
            {
            match("type"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Type"

    // $ANTLR start "Uint"
    public final void mUint() throws RecognitionException {
        try {
            int _type = Uint;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:98:6: ( 'uint' )
            // InternalOSC2Lexer.g:98:8: 'uint'
            {
            match("uint"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Uint"

    // $ANTLR start "Unit"
    public final void mUnit() throws RecognitionException {
        try {
            int _type = Unit;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:100:6: ( 'unit' )
            // InternalOSC2Lexer.g:100:8: 'unit'
            {
            match("unit"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Unit"

    // $ANTLR start "Wait"
    public final void mWait() throws RecognitionException {
        try {
            int _type = Wait;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:102:6: ( 'wait' )
            // InternalOSC2Lexer.g:102:8: 'wait'
            {
            match("wait"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Wait"

    // $ANTLR start "With"
    public final void mWith() throws RecognitionException {
        try {
            int _type = With;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:104:6: ( 'with' )
            // InternalOSC2Lexer.g:104:8: 'with'
            {
            match("with"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "With"

    // $ANTLR start "And"
    public final void mAnd() throws RecognitionException {
        try {
            int _type = And;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:106:5: ( 'and' )
            // InternalOSC2Lexer.g:106:7: 'and'
            {
            match("and"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "And"

    // $ANTLR start "Def"
    public final void mDef() throws RecognitionException {
        try {
            int _type = Def;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:108:5: ( 'def' )
            // InternalOSC2Lexer.g:108:7: 'def'
            {
            match("def"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Def"

    // $ANTLR start "Int"
    public final void mInt() throws RecognitionException {
        try {
            int _type = Int;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:110:5: ( 'int' )
            // InternalOSC2Lexer.g:110:7: 'int'
            {
            match("int"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Int"

    // $ANTLR start "Mol"
    public final void mMol() throws RecognitionException {
        try {
            int _type = Mol;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:112:5: ( 'mol' )
            // InternalOSC2Lexer.g:112:7: 'mol'
            {
            match("mol"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Mol"

    // $ANTLR start "Not"
    public final void mNot() throws RecognitionException {
        try {
            int _type = Not;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:114:5: ( 'not' )
            // InternalOSC2Lexer.g:114:7: 'not'
            {
            match("not"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Not"

    // $ANTLR start "Rad"
    public final void mRad() throws RecognitionException {
        try {
            int _type = Rad;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:116:5: ( 'rad' )
            // InternalOSC2Lexer.g:116:7: 'rad'
            {
            match("rad"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Rad"

    // $ANTLR start "Var"
    public final void mVar() throws RecognitionException {
        try {
            int _type = Var;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:118:5: ( 'var' )
            // InternalOSC2Lexer.g:118:7: 'var'
            {
            match("var"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Var"

    // $ANTLR start "ExclamationMarkEqualsSign"
    public final void mExclamationMarkEqualsSign() throws RecognitionException {
        try {
            int _type = ExclamationMarkEqualsSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:120:27: ( '!=' )
            // InternalOSC2Lexer.g:120:29: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ExclamationMarkEqualsSign"

    // $ANTLR start "HyphenMinusGreaterThanSign"
    public final void mHyphenMinusGreaterThanSign() throws RecognitionException {
        try {
            int _type = HyphenMinusGreaterThanSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:122:28: ( '->' )
            // InternalOSC2Lexer.g:122:30: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "HyphenMinusGreaterThanSign"

    // $ANTLR start "FullStopFullStop"
    public final void mFullStopFullStop() throws RecognitionException {
        try {
            int _type = FullStopFullStop;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:124:18: ( '..' )
            // InternalOSC2Lexer.g:124:20: '..'
            {
            match(".."); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "FullStopFullStop"

    // $ANTLR start "LessThanSignEqualsSign"
    public final void mLessThanSignEqualsSign() throws RecognitionException {
        try {
            int _type = LessThanSignEqualsSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:126:24: ( '<=' )
            // InternalOSC2Lexer.g:126:26: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LessThanSignEqualsSign"

    // $ANTLR start "EqualsSignEqualsSign"
    public final void mEqualsSignEqualsSign() throws RecognitionException {
        try {
            int _type = EqualsSignEqualsSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:128:22: ( '==' )
            // InternalOSC2Lexer.g:128:24: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EqualsSignEqualsSign"

    // $ANTLR start "EqualsSignGreaterThanSign"
    public final void mEqualsSignGreaterThanSign() throws RecognitionException {
        try {
            int _type = EqualsSignGreaterThanSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:130:27: ( '=>' )
            // InternalOSC2Lexer.g:130:29: '=>'
            {
            match("=>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EqualsSignGreaterThanSign"

    // $ANTLR start "GreaterThanSignEqualsSign"
    public final void mGreaterThanSignEqualsSign() throws RecognitionException {
        try {
            int _type = GreaterThanSignEqualsSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:132:27: ( '>=' )
            // InternalOSC2Lexer.g:132:29: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GreaterThanSignEqualsSign"

    // $ANTLR start "SI"
    public final void mSI() throws RecognitionException {
        try {
            int _type = SI;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:134:4: ( 'SI' )
            // InternalOSC2Lexer.g:134:6: 'SI'
            {
            match("SI"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SI"

    // $ANTLR start "As"
    public final void mAs() throws RecognitionException {
        try {
            int _type = As;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:136:4: ( 'as' )
            // InternalOSC2Lexer.g:136:6: 'as'
            {
            match("as"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "As"

    // $ANTLR start "Cd"
    public final void mCd() throws RecognitionException {
        try {
            int _type = Cd;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:138:4: ( 'cd' )
            // InternalOSC2Lexer.g:138:6: 'cd'
            {
            match("cd"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Cd"

    // $ANTLR start "Do"
    public final void mDo() throws RecognitionException {
        try {
            int _type = Do;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:140:4: ( 'do' )
            // InternalOSC2Lexer.g:140:6: 'do'
            {
            match("do"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Do"

    // $ANTLR start "If"
    public final void mIf() throws RecognitionException {
        try {
            int _type = If;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:142:4: ( 'if' )
            // InternalOSC2Lexer.g:142:6: 'if'
            {
            match("if"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "If"

    // $ANTLR start "In"
    public final void mIn() throws RecognitionException {
        try {
            int _type = In;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:144:4: ( 'in' )
            // InternalOSC2Lexer.g:144:6: 'in'
            {
            match("in"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "In"

    // $ANTLR start "Is"
    public final void mIs() throws RecognitionException {
        try {
            int _type = Is;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:146:4: ( 'is' )
            // InternalOSC2Lexer.g:146:6: 'is'
            {
            match("is"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Is"

    // $ANTLR start "It"
    public final void mIt() throws RecognitionException {
        try {
            int _type = It;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:148:4: ( 'it' )
            // InternalOSC2Lexer.g:148:6: 'it'
            {
            match("it"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "It"

    // $ANTLR start "Kg"
    public final void mKg() throws RecognitionException {
        try {
            int _type = Kg;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:150:4: ( 'kg' )
            // InternalOSC2Lexer.g:150:6: 'kg'
            {
            match("kg"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Kg"

    // $ANTLR start "Of"
    public final void mOf() throws RecognitionException {
        try {
            int _type = Of;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:152:4: ( 'of' )
            // InternalOSC2Lexer.g:152:6: 'of'
            {
            match("of"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Of"

    // $ANTLR start "On"
    public final void mOn() throws RecognitionException {
        try {
            int _type = On;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:154:4: ( 'on' )
            // InternalOSC2Lexer.g:154:6: 'on'
            {
            match("on"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "On"

    // $ANTLR start "Or"
    public final void mOr() throws RecognitionException {
        try {
            int _type = Or;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:156:4: ( 'or' )
            // InternalOSC2Lexer.g:156:6: 'or'
            {
            match("or"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Or"

    // $ANTLR start "ExclamationMark"
    public final void mExclamationMark() throws RecognitionException {
        try {
            int _type = ExclamationMark;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:158:17: ( '!' )
            // InternalOSC2Lexer.g:158:19: '!'
            {
            match('!'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ExclamationMark"

    // $ANTLR start "PercentSign"
    public final void mPercentSign() throws RecognitionException {
        try {
            int _type = PercentSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:160:13: ( '%' )
            // InternalOSC2Lexer.g:160:15: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PercentSign"

    // $ANTLR start "LeftParenthesis"
    public final void mLeftParenthesis() throws RecognitionException {
        try {
            int _type = LeftParenthesis;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:162:17: ( '(' )
            // InternalOSC2Lexer.g:162:19: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LeftParenthesis"

    // $ANTLR start "RightParenthesis"
    public final void mRightParenthesis() throws RecognitionException {
        try {
            int _type = RightParenthesis;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:164:18: ( ')' )
            // InternalOSC2Lexer.g:164:20: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RightParenthesis"

    // $ANTLR start "Asterisk"
    public final void mAsterisk() throws RecognitionException {
        try {
            int _type = Asterisk;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:166:10: ( '*' )
            // InternalOSC2Lexer.g:166:12: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Asterisk"

    // $ANTLR start "PlusSign"
    public final void mPlusSign() throws RecognitionException {
        try {
            int _type = PlusSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:168:10: ( '+' )
            // InternalOSC2Lexer.g:168:12: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PlusSign"

    // $ANTLR start "Comma"
    public final void mComma() throws RecognitionException {
        try {
            int _type = Comma;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:170:7: ( ',' )
            // InternalOSC2Lexer.g:170:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Comma"

    // $ANTLR start "HyphenMinus"
    public final void mHyphenMinus() throws RecognitionException {
        try {
            int _type = HyphenMinus;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:172:13: ( '-' )
            // InternalOSC2Lexer.g:172:15: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "HyphenMinus"

    // $ANTLR start "FullStop"
    public final void mFullStop() throws RecognitionException {
        try {
            int _type = FullStop;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:174:10: ( '.' )
            // InternalOSC2Lexer.g:174:12: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "FullStop"

    // $ANTLR start "Solidus"
    public final void mSolidus() throws RecognitionException {
        try {
            int _type = Solidus;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:176:9: ( '/' )
            // InternalOSC2Lexer.g:176:11: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Solidus"

    // $ANTLR start "Colon"
    public final void mColon() throws RecognitionException {
        try {
            int _type = Colon;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:178:7: ( ':' )
            // InternalOSC2Lexer.g:178:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "Colon"

    // $ANTLR start "LessThanSign"
    public final void mLessThanSign() throws RecognitionException {
        try {
            int _type = LessThanSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:180:14: ( '<' )
            // InternalOSC2Lexer.g:180:16: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LessThanSign"

    // $ANTLR start "EqualsSign"
    public final void mEqualsSign() throws RecognitionException {
        try {
            int _type = EqualsSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:182:12: ( '=' )
            // InternalOSC2Lexer.g:182:14: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EqualsSign"

    // $ANTLR start "GreaterThanSign"
    public final void mGreaterThanSign() throws RecognitionException {
        try {
            int _type = GreaterThanSign;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:184:17: ( '>' )
            // InternalOSC2Lexer.g:184:19: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GreaterThanSign"

    // $ANTLR start "QuestionMark"
    public final void mQuestionMark() throws RecognitionException {
        try {
            int _type = QuestionMark;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:186:14: ( '?' )
            // InternalOSC2Lexer.g:186:16: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "QuestionMark"

    // $ANTLR start "CommercialAt"
    public final void mCommercialAt() throws RecognitionException {
        try {
            int _type = CommercialAt;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:188:14: ( '@' )
            // InternalOSC2Lexer.g:188:16: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "CommercialAt"

    // $ANTLR start "A"
    public final void mA() throws RecognitionException {
        try {
            int _type = A;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:190:3: ( 'A' )
            // InternalOSC2Lexer.g:190:5: 'A'
            {
            match('A'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "A"

    // $ANTLR start "K"
    public final void mK() throws RecognitionException {
        try {
            int _type = K;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:192:3: ( 'K' )
            // InternalOSC2Lexer.g:192:5: 'K'
            {
            match('K'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "K"

    // $ANTLR start "LeftSquareBracket"
    public final void mLeftSquareBracket() throws RecognitionException {
        try {
            int _type = LeftSquareBracket;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:194:19: ( '[' )
            // InternalOSC2Lexer.g:194:21: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LeftSquareBracket"

    // $ANTLR start "RightSquareBracket"
    public final void mRightSquareBracket() throws RecognitionException {
        try {
            int _type = RightSquareBracket;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:196:20: ( ']' )
            // InternalOSC2Lexer.g:196:22: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RightSquareBracket"

    // $ANTLR start "KW__"
    public final void mKW__() throws RecognitionException {
        try {
            int _type = KW__;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:198:6: ( '_' )
            // InternalOSC2Lexer.g:198:8: '_'
            {
            match('_'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "KW__"

    // $ANTLR start "M"
    public final void mM() throws RecognitionException {
        try {
            int _type = M;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:200:3: ( 'm' )
            // InternalOSC2Lexer.g:200:5: 'm'
            {
            match('m'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "M"

    // $ANTLR start "S"
    public final void mS() throws RecognitionException {
        try {
            int _type = S;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:202:3: ( 's' )
            // InternalOSC2Lexer.g:202:5: 's'
            {
            match('s'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "S"

    // $ANTLR start "RULE_BEGIN"
    public final void mRULE_BEGIN() throws RecognitionException {
        try {
            // InternalOSC2Lexer.g:204:21: ()
            // InternalOSC2Lexer.g:204:23: 
            {
            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_BEGIN"

    // $ANTLR start "RULE_END"
    public final void mRULE_END() throws RecognitionException {
        try {
            // InternalOSC2Lexer.g:206:19: ()
            // InternalOSC2Lexer.g:206:21: 
            {
            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_END"

    // $ANTLR start "RULE_IDENTIFIER"
    public final void mRULE_IDENTIFIER() throws RecognitionException {
        try {
            int _type = RULE_IDENTIFIER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:208:17: ( ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* | '|' (~ ( '|' ) )+ '|' ) )
            // InternalOSC2Lexer.g:208:19: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* | '|' (~ ( '|' ) )+ '|' )
            {
            // InternalOSC2Lexer.g:208:19: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* | '|' (~ ( '|' ) )+ '|' )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( ((LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                alt3=1;
            }
            else if ( (LA3_0=='|') ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalOSC2Lexer.g:208:20: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
                    {
                    if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // InternalOSC2Lexer.g:208:44: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
                    loop1:
                    do {
                        int alt1=2;
                        int LA1_0 = input.LA(1);

                        if ( ((LA1_0>='0' && LA1_0<='9')||(LA1_0>='A' && LA1_0<='Z')||LA1_0=='_'||(LA1_0>='a' && LA1_0<='z')) ) {
                            alt1=1;
                        }


                        switch (alt1) {
                    	case 1 :
                    	    // InternalOSC2Lexer.g:
                    	    {
                    	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop1;
                        }
                    } while (true);


                    }
                    break;
                case 2 :
                    // InternalOSC2Lexer.g:208:78: '|' (~ ( '|' ) )+ '|'
                    {
                    match('|'); 
                    // InternalOSC2Lexer.g:208:82: (~ ( '|' ) )+
                    int cnt2=0;
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( ((LA2_0>='\u0000' && LA2_0<='{')||(LA2_0>='}' && LA2_0<='\uFFFF')) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // InternalOSC2Lexer.g:208:82: ~ ( '|' )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='{')||(input.LA(1)>='}' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt2 >= 1 ) break loop2;
                                EarlyExitException eee =
                                    new EarlyExitException(2, input);
                                throw eee;
                        }
                        cnt2++;
                    } while (true);

                    match('|'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_IDENTIFIER"

    // $ANTLR start "RULE_DIGIT"
    public final void mRULE_DIGIT() throws RecognitionException {
        try {
            // InternalOSC2Lexer.g:210:21: ( ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' ) )
            // InternalOSC2Lexer.g:210:23: ( '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_DIGIT"

    // $ANTLR start "RULE_UINT"
    public final void mRULE_UINT() throws RecognitionException {
        try {
            int _type = RULE_UINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:212:11: ( ( RULE_DIGIT )+ )
            // InternalOSC2Lexer.g:212:13: ( RULE_DIGIT )+
            {
            // InternalOSC2Lexer.g:212:13: ( RULE_DIGIT )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>='0' && LA4_0<='9')) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalOSC2Lexer.g:212:13: RULE_DIGIT
            	    {
            	    mRULE_DIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_UINT"

    // $ANTLR start "RULE_HEX_UINT"
    public final void mRULE_HEX_UINT() throws RecognitionException {
        try {
            int _type = RULE_HEX_UINT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:214:15: ( '0x' ( RULE_HEX_DIGIT )+ )
            // InternalOSC2Lexer.g:214:17: '0x' ( RULE_HEX_DIGIT )+
            {
            match("0x"); 

            // InternalOSC2Lexer.g:214:22: ( RULE_HEX_DIGIT )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( ((LA5_0>='0' && LA5_0<='9')||(LA5_0>='A' && LA5_0<='F')||(LA5_0>='a' && LA5_0<='f')) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // InternalOSC2Lexer.g:214:22: RULE_HEX_DIGIT
            	    {
            	    mRULE_HEX_DIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEX_UINT"

    // $ANTLR start "RULE_NEG_INT"
    public final void mRULE_NEG_INT() throws RecognitionException {
        try {
            int _type = RULE_NEG_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:216:14: ( '-' ( RULE_DIGIT )+ )
            // InternalOSC2Lexer.g:216:16: '-' ( RULE_DIGIT )+
            {
            match('-'); 
            // InternalOSC2Lexer.g:216:20: ( RULE_DIGIT )+
            int cnt6=0;
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( ((LA6_0>='0' && LA6_0<='9')) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalOSC2Lexer.g:216:20: RULE_DIGIT
            	    {
            	    mRULE_DIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt6 >= 1 ) break loop6;
                        EarlyExitException eee =
                            new EarlyExitException(6, input);
                        throw eee;
                }
                cnt6++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_NEG_INT"

    // $ANTLR start "RULE_HEX_DIGIT"
    public final void mRULE_HEX_DIGIT() throws RecognitionException {
        try {
            // InternalOSC2Lexer.g:218:25: ( ( RULE_DIGIT | 'A' | 'a' | 'B' | 'b' | 'C' | 'c' | 'D' | 'd' | 'E' | 'e' | 'F' | 'f' ) )
            // InternalOSC2Lexer.g:218:27: ( RULE_DIGIT | 'A' | 'a' | 'B' | 'b' | 'C' | 'c' | 'D' | 'd' | 'E' | 'e' | 'F' | 'f' )
            {
            if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='F')||(input.LA(1)>='a' && input.LA(1)<='f') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}


            }

        }
        finally {
        }
    }
    // $ANTLR end "RULE_HEX_DIGIT"

    // $ANTLR start "RULE_FLOAT"
    public final void mRULE_FLOAT() throws RecognitionException {
        try {
            int _type = RULE_FLOAT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:220:12: ( ( '+' | '-' )? ( RULE_DIGIT )* '.' ( RULE_DIGIT )+ ( ( 'e' | 'E' ) ( '+' | '-' )? ( RULE_DIGIT )+ )? )
            // InternalOSC2Lexer.g:220:14: ( '+' | '-' )? ( RULE_DIGIT )* '.' ( RULE_DIGIT )+ ( ( 'e' | 'E' ) ( '+' | '-' )? ( RULE_DIGIT )+ )?
            {
            // InternalOSC2Lexer.g:220:14: ( '+' | '-' )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0=='+'||LA7_0=='-') ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // InternalOSC2Lexer.g:
                    {
                    if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}


                    }
                    break;

            }

            // InternalOSC2Lexer.g:220:25: ( RULE_DIGIT )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='0' && LA8_0<='9')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalOSC2Lexer.g:220:25: RULE_DIGIT
            	    {
            	    mRULE_DIGIT(); 

            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            match('.'); 
            // InternalOSC2Lexer.g:220:41: ( RULE_DIGIT )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( ((LA9_0>='0' && LA9_0<='9')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalOSC2Lexer.g:220:41: RULE_DIGIT
            	    {
            	    mRULE_DIGIT(); 

            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);

            // InternalOSC2Lexer.g:220:53: ( ( 'e' | 'E' ) ( '+' | '-' )? ( RULE_DIGIT )+ )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='E'||LA12_0=='e') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalOSC2Lexer.g:220:54: ( 'e' | 'E' ) ( '+' | '-' )? ( RULE_DIGIT )+
                    {
                    if ( input.LA(1)=='E'||input.LA(1)=='e' ) {
                        input.consume();

                    }
                    else {
                        MismatchedSetException mse = new MismatchedSetException(null,input);
                        recover(mse);
                        throw mse;}

                    // InternalOSC2Lexer.g:220:64: ( '+' | '-' )?
                    int alt10=2;
                    int LA10_0 = input.LA(1);

                    if ( (LA10_0=='+'||LA10_0=='-') ) {
                        alt10=1;
                    }
                    switch (alt10) {
                        case 1 :
                            // InternalOSC2Lexer.g:
                            {
                            if ( input.LA(1)=='+'||input.LA(1)=='-' ) {
                                input.consume();

                            }
                            else {
                                MismatchedSetException mse = new MismatchedSetException(null,input);
                                recover(mse);
                                throw mse;}


                            }
                            break;

                    }

                    // InternalOSC2Lexer.g:220:75: ( RULE_DIGIT )+
                    int cnt11=0;
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( ((LA11_0>='0' && LA11_0<='9')) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // InternalOSC2Lexer.g:220:75: RULE_DIGIT
                    	    {
                    	    mRULE_DIGIT(); 

                    	    }
                    	    break;

                    	default :
                    	    if ( cnt11 >= 1 ) break loop11;
                                EarlyExitException eee =
                                    new EarlyExitException(11, input);
                                throw eee;
                        }
                        cnt11++;
                    } while (true);


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FLOAT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:222:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\\'' ) )
            // InternalOSC2Lexer.g:222:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\\'' )
            {
            // InternalOSC2Lexer.g:222:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\\'' )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0=='\"') ) {
                alt15=1;
            }
            else if ( (LA15_0=='\'') ) {
                alt15=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalOSC2Lexer.g:222:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalOSC2Lexer.g:222:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )*
                    loop13:
                    do {
                        int alt13=3;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0=='\\') ) {
                            alt13=1;
                        }
                        else if ( ((LA13_0>='\u0000' && LA13_0<='\t')||(LA13_0>='\u000B' && LA13_0<='\f')||(LA13_0>='\u000E' && LA13_0<='!')||(LA13_0>='#' && LA13_0<='[')||(LA13_0>=']' && LA13_0<='\uFFFF')) ) {
                            alt13=2;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // InternalOSC2Lexer.g:222:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOSC2Lexer.g:222:28: ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalOSC2Lexer.g:222:58: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalOSC2Lexer.g:222:63: ( '\\\\' . | ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) ) )*
                    loop14:
                    do {
                        int alt14=3;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0=='\'') ) {
                            int LA14_1 = input.LA(2);

                            if ( ((LA14_1>='\u0000' && LA14_1<='\t')||(LA14_1>='\u000B' && LA14_1<='\f')||(LA14_1>='\u000E' && LA14_1<='!')||(LA14_1>='#' && LA14_1<='\uFFFF')) ) {
                                alt14=2;
                            }


                        }
                        else if ( (LA14_0=='\\') ) {
                            alt14=1;
                        }
                        else if ( ((LA14_0>='\u0000' && LA14_0<='\t')||(LA14_0>='\u000B' && LA14_0<='\f')||(LA14_0>='\u000E' && LA14_0<='!')||(LA14_0>='#' && LA14_0<='&')||(LA14_0>='(' && LA14_0<='[')||(LA14_0>=']' && LA14_0<='\uFFFF')) ) {
                            alt14=2;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // InternalOSC2Lexer.g:222:64: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalOSC2Lexer.g:222:71: ~ ( ( '\\\\' | '\"' | '\\n' | '\\r' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:224:17: ( '#' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalOSC2Lexer.g:224:19: '#' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match('#'); 
            // InternalOSC2Lexer.g:224:23: (~ ( ( '\\n' | '\\r' ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( ((LA16_0>='\u0000' && LA16_0<='\t')||(LA16_0>='\u000B' && LA16_0<='\f')||(LA16_0>='\u000E' && LA16_0<='\uFFFF')) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalOSC2Lexer.g:224:23: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);

            // InternalOSC2Lexer.g:224:39: ( ( '\\r' )? '\\n' )?
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0=='\n'||LA18_0=='\r') ) {
                alt18=1;
            }
            switch (alt18) {
                case 1 :
                    // InternalOSC2Lexer.g:224:40: ( '\\r' )? '\\n'
                    {
                    // InternalOSC2Lexer.g:224:40: ( '\\r' )?
                    int alt17=2;
                    int LA17_0 = input.LA(1);

                    if ( (LA17_0=='\r') ) {
                        alt17=1;
                    }
                    switch (alt17) {
                        case 1 :
                            // InternalOSC2Lexer.g:224:40: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:226:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalOSC2Lexer.g:226:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalOSC2Lexer.g:226:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt19=0;
            loop19:
            do {
                int alt19=2;
                int LA19_0 = input.LA(1);

                if ( ((LA19_0>='\t' && LA19_0<='\n')||LA19_0=='\r'||LA19_0==' ') ) {
                    alt19=1;
                }


                switch (alt19) {
            	case 1 :
            	    // InternalOSC2Lexer.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt19 >= 1 ) break loop19;
                        EarlyExitException eee =
                            new EarlyExitException(19, input);
                        throw eee;
                }
                cnt19++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalOSC2Lexer.g:228:16: ( . )
            // InternalOSC2Lexer.g:228:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalOSC2Lexer.g:1:8: ( Remove_default | Expression | Undefined | External | Inherits | Modifier | Parallel | Scenario | Default | Elapsed | Action | Extend | Factor | Global | Import | Offset | One_of | Record | Sample | Serial | String | Struct | Actor | Cover | Event | Every | False | Float | Range | Until | Bool | Call | Emit | Enum | Fall | Hard | Keep | List | Only | Rise | True | Type | Uint | Unit | Wait | With | And | Def | Int | Mol | Not | Rad | Var | ExclamationMarkEqualsSign | HyphenMinusGreaterThanSign | FullStopFullStop | LessThanSignEqualsSign | EqualsSignEqualsSign | EqualsSignGreaterThanSign | GreaterThanSignEqualsSign | SI | As | Cd | Do | If | In | Is | It | Kg | Of | On | Or | ExclamationMark | PercentSign | LeftParenthesis | RightParenthesis | Asterisk | PlusSign | Comma | HyphenMinus | FullStop | Solidus | Colon | LessThanSign | EqualsSign | GreaterThanSign | QuestionMark | CommercialAt | A | K | LeftSquareBracket | RightSquareBracket | KW__ | M | S | RULE_IDENTIFIER | RULE_UINT | RULE_HEX_UINT | RULE_NEG_INT | RULE_FLOAT | RULE_STRING | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt20=104;
        alt20 = dfa20.predict(input);
        switch (alt20) {
            case 1 :
                // InternalOSC2Lexer.g:1:10: Remove_default
                {
                mRemove_default(); 

                }
                break;
            case 2 :
                // InternalOSC2Lexer.g:1:25: Expression
                {
                mExpression(); 

                }
                break;
            case 3 :
                // InternalOSC2Lexer.g:1:36: Undefined
                {
                mUndefined(); 

                }
                break;
            case 4 :
                // InternalOSC2Lexer.g:1:46: External
                {
                mExternal(); 

                }
                break;
            case 5 :
                // InternalOSC2Lexer.g:1:55: Inherits
                {
                mInherits(); 

                }
                break;
            case 6 :
                // InternalOSC2Lexer.g:1:64: Modifier
                {
                mModifier(); 

                }
                break;
            case 7 :
                // InternalOSC2Lexer.g:1:73: Parallel
                {
                mParallel(); 

                }
                break;
            case 8 :
                // InternalOSC2Lexer.g:1:82: Scenario
                {
                mScenario(); 

                }
                break;
            case 9 :
                // InternalOSC2Lexer.g:1:91: Default
                {
                mDefault(); 

                }
                break;
            case 10 :
                // InternalOSC2Lexer.g:1:99: Elapsed
                {
                mElapsed(); 

                }
                break;
            case 11 :
                // InternalOSC2Lexer.g:1:107: Action
                {
                mAction(); 

                }
                break;
            case 12 :
                // InternalOSC2Lexer.g:1:114: Extend
                {
                mExtend(); 

                }
                break;
            case 13 :
                // InternalOSC2Lexer.g:1:121: Factor
                {
                mFactor(); 

                }
                break;
            case 14 :
                // InternalOSC2Lexer.g:1:128: Global
                {
                mGlobal(); 

                }
                break;
            case 15 :
                // InternalOSC2Lexer.g:1:135: Import
                {
                mImport(); 

                }
                break;
            case 16 :
                // InternalOSC2Lexer.g:1:142: Offset
                {
                mOffset(); 

                }
                break;
            case 17 :
                // InternalOSC2Lexer.g:1:149: One_of
                {
                mOne_of(); 

                }
                break;
            case 18 :
                // InternalOSC2Lexer.g:1:156: Record
                {
                mRecord(); 

                }
                break;
            case 19 :
                // InternalOSC2Lexer.g:1:163: Sample
                {
                mSample(); 

                }
                break;
            case 20 :
                // InternalOSC2Lexer.g:1:170: Serial
                {
                mSerial(); 

                }
                break;
            case 21 :
                // InternalOSC2Lexer.g:1:177: String
                {
                mString(); 

                }
                break;
            case 22 :
                // InternalOSC2Lexer.g:1:184: Struct
                {
                mStruct(); 

                }
                break;
            case 23 :
                // InternalOSC2Lexer.g:1:191: Actor
                {
                mActor(); 

                }
                break;
            case 24 :
                // InternalOSC2Lexer.g:1:197: Cover
                {
                mCover(); 

                }
                break;
            case 25 :
                // InternalOSC2Lexer.g:1:203: Event
                {
                mEvent(); 

                }
                break;
            case 26 :
                // InternalOSC2Lexer.g:1:209: Every
                {
                mEvery(); 

                }
                break;
            case 27 :
                // InternalOSC2Lexer.g:1:215: False
                {
                mFalse(); 

                }
                break;
            case 28 :
                // InternalOSC2Lexer.g:1:221: Float
                {
                mFloat(); 

                }
                break;
            case 29 :
                // InternalOSC2Lexer.g:1:227: Range
                {
                mRange(); 

                }
                break;
            case 30 :
                // InternalOSC2Lexer.g:1:233: Until
                {
                mUntil(); 

                }
                break;
            case 31 :
                // InternalOSC2Lexer.g:1:239: Bool
                {
                mBool(); 

                }
                break;
            case 32 :
                // InternalOSC2Lexer.g:1:244: Call
                {
                mCall(); 

                }
                break;
            case 33 :
                // InternalOSC2Lexer.g:1:249: Emit
                {
                mEmit(); 

                }
                break;
            case 34 :
                // InternalOSC2Lexer.g:1:254: Enum
                {
                mEnum(); 

                }
                break;
            case 35 :
                // InternalOSC2Lexer.g:1:259: Fall
                {
                mFall(); 

                }
                break;
            case 36 :
                // InternalOSC2Lexer.g:1:264: Hard
                {
                mHard(); 

                }
                break;
            case 37 :
                // InternalOSC2Lexer.g:1:269: Keep
                {
                mKeep(); 

                }
                break;
            case 38 :
                // InternalOSC2Lexer.g:1:274: List
                {
                mList(); 

                }
                break;
            case 39 :
                // InternalOSC2Lexer.g:1:279: Only
                {
                mOnly(); 

                }
                break;
            case 40 :
                // InternalOSC2Lexer.g:1:284: Rise
                {
                mRise(); 

                }
                break;
            case 41 :
                // InternalOSC2Lexer.g:1:289: True
                {
                mTrue(); 

                }
                break;
            case 42 :
                // InternalOSC2Lexer.g:1:294: Type
                {
                mType(); 

                }
                break;
            case 43 :
                // InternalOSC2Lexer.g:1:299: Uint
                {
                mUint(); 

                }
                break;
            case 44 :
                // InternalOSC2Lexer.g:1:304: Unit
                {
                mUnit(); 

                }
                break;
            case 45 :
                // InternalOSC2Lexer.g:1:309: Wait
                {
                mWait(); 

                }
                break;
            case 46 :
                // InternalOSC2Lexer.g:1:314: With
                {
                mWith(); 

                }
                break;
            case 47 :
                // InternalOSC2Lexer.g:1:319: And
                {
                mAnd(); 

                }
                break;
            case 48 :
                // InternalOSC2Lexer.g:1:323: Def
                {
                mDef(); 

                }
                break;
            case 49 :
                // InternalOSC2Lexer.g:1:327: Int
                {
                mInt(); 

                }
                break;
            case 50 :
                // InternalOSC2Lexer.g:1:331: Mol
                {
                mMol(); 

                }
                break;
            case 51 :
                // InternalOSC2Lexer.g:1:335: Not
                {
                mNot(); 

                }
                break;
            case 52 :
                // InternalOSC2Lexer.g:1:339: Rad
                {
                mRad(); 

                }
                break;
            case 53 :
                // InternalOSC2Lexer.g:1:343: Var
                {
                mVar(); 

                }
                break;
            case 54 :
                // InternalOSC2Lexer.g:1:347: ExclamationMarkEqualsSign
                {
                mExclamationMarkEqualsSign(); 

                }
                break;
            case 55 :
                // InternalOSC2Lexer.g:1:373: HyphenMinusGreaterThanSign
                {
                mHyphenMinusGreaterThanSign(); 

                }
                break;
            case 56 :
                // InternalOSC2Lexer.g:1:400: FullStopFullStop
                {
                mFullStopFullStop(); 

                }
                break;
            case 57 :
                // InternalOSC2Lexer.g:1:417: LessThanSignEqualsSign
                {
                mLessThanSignEqualsSign(); 

                }
                break;
            case 58 :
                // InternalOSC2Lexer.g:1:440: EqualsSignEqualsSign
                {
                mEqualsSignEqualsSign(); 

                }
                break;
            case 59 :
                // InternalOSC2Lexer.g:1:461: EqualsSignGreaterThanSign
                {
                mEqualsSignGreaterThanSign(); 

                }
                break;
            case 60 :
                // InternalOSC2Lexer.g:1:487: GreaterThanSignEqualsSign
                {
                mGreaterThanSignEqualsSign(); 

                }
                break;
            case 61 :
                // InternalOSC2Lexer.g:1:513: SI
                {
                mSI(); 

                }
                break;
            case 62 :
                // InternalOSC2Lexer.g:1:516: As
                {
                mAs(); 

                }
                break;
            case 63 :
                // InternalOSC2Lexer.g:1:519: Cd
                {
                mCd(); 

                }
                break;
            case 64 :
                // InternalOSC2Lexer.g:1:522: Do
                {
                mDo(); 

                }
                break;
            case 65 :
                // InternalOSC2Lexer.g:1:525: If
                {
                mIf(); 

                }
                break;
            case 66 :
                // InternalOSC2Lexer.g:1:528: In
                {
                mIn(); 

                }
                break;
            case 67 :
                // InternalOSC2Lexer.g:1:531: Is
                {
                mIs(); 

                }
                break;
            case 68 :
                // InternalOSC2Lexer.g:1:534: It
                {
                mIt(); 

                }
                break;
            case 69 :
                // InternalOSC2Lexer.g:1:537: Kg
                {
                mKg(); 

                }
                break;
            case 70 :
                // InternalOSC2Lexer.g:1:540: Of
                {
                mOf(); 

                }
                break;
            case 71 :
                // InternalOSC2Lexer.g:1:543: On
                {
                mOn(); 

                }
                break;
            case 72 :
                // InternalOSC2Lexer.g:1:546: Or
                {
                mOr(); 

                }
                break;
            case 73 :
                // InternalOSC2Lexer.g:1:549: ExclamationMark
                {
                mExclamationMark(); 

                }
                break;
            case 74 :
                // InternalOSC2Lexer.g:1:565: PercentSign
                {
                mPercentSign(); 

                }
                break;
            case 75 :
                // InternalOSC2Lexer.g:1:577: LeftParenthesis
                {
                mLeftParenthesis(); 

                }
                break;
            case 76 :
                // InternalOSC2Lexer.g:1:593: RightParenthesis
                {
                mRightParenthesis(); 

                }
                break;
            case 77 :
                // InternalOSC2Lexer.g:1:610: Asterisk
                {
                mAsterisk(); 

                }
                break;
            case 78 :
                // InternalOSC2Lexer.g:1:619: PlusSign
                {
                mPlusSign(); 

                }
                break;
            case 79 :
                // InternalOSC2Lexer.g:1:628: Comma
                {
                mComma(); 

                }
                break;
            case 80 :
                // InternalOSC2Lexer.g:1:634: HyphenMinus
                {
                mHyphenMinus(); 

                }
                break;
            case 81 :
                // InternalOSC2Lexer.g:1:646: FullStop
                {
                mFullStop(); 

                }
                break;
            case 82 :
                // InternalOSC2Lexer.g:1:655: Solidus
                {
                mSolidus(); 

                }
                break;
            case 83 :
                // InternalOSC2Lexer.g:1:663: Colon
                {
                mColon(); 

                }
                break;
            case 84 :
                // InternalOSC2Lexer.g:1:669: LessThanSign
                {
                mLessThanSign(); 

                }
                break;
            case 85 :
                // InternalOSC2Lexer.g:1:682: EqualsSign
                {
                mEqualsSign(); 

                }
                break;
            case 86 :
                // InternalOSC2Lexer.g:1:693: GreaterThanSign
                {
                mGreaterThanSign(); 

                }
                break;
            case 87 :
                // InternalOSC2Lexer.g:1:709: QuestionMark
                {
                mQuestionMark(); 

                }
                break;
            case 88 :
                // InternalOSC2Lexer.g:1:722: CommercialAt
                {
                mCommercialAt(); 

                }
                break;
            case 89 :
                // InternalOSC2Lexer.g:1:735: A
                {
                mA(); 

                }
                break;
            case 90 :
                // InternalOSC2Lexer.g:1:737: K
                {
                mK(); 

                }
                break;
            case 91 :
                // InternalOSC2Lexer.g:1:739: LeftSquareBracket
                {
                mLeftSquareBracket(); 

                }
                break;
            case 92 :
                // InternalOSC2Lexer.g:1:757: RightSquareBracket
                {
                mRightSquareBracket(); 

                }
                break;
            case 93 :
                // InternalOSC2Lexer.g:1:776: KW__
                {
                mKW__(); 

                }
                break;
            case 94 :
                // InternalOSC2Lexer.g:1:781: M
                {
                mM(); 

                }
                break;
            case 95 :
                // InternalOSC2Lexer.g:1:783: S
                {
                mS(); 

                }
                break;
            case 96 :
                // InternalOSC2Lexer.g:1:785: RULE_IDENTIFIER
                {
                mRULE_IDENTIFIER(); 

                }
                break;
            case 97 :
                // InternalOSC2Lexer.g:1:801: RULE_UINT
                {
                mRULE_UINT(); 

                }
                break;
            case 98 :
                // InternalOSC2Lexer.g:1:811: RULE_HEX_UINT
                {
                mRULE_HEX_UINT(); 

                }
                break;
            case 99 :
                // InternalOSC2Lexer.g:1:825: RULE_NEG_INT
                {
                mRULE_NEG_INT(); 

                }
                break;
            case 100 :
                // InternalOSC2Lexer.g:1:838: RULE_FLOAT
                {
                mRULE_FLOAT(); 

                }
                break;
            case 101 :
                // InternalOSC2Lexer.g:1:849: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 102 :
                // InternalOSC2Lexer.g:1:861: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 103 :
                // InternalOSC2Lexer.g:1:877: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 104 :
                // InternalOSC2Lexer.g:1:885: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA20 dfa20 = new DFA20(this);
    static final String DFA20_eotS =
        "\1\uffff\4\70\1\106\1\70\1\114\16\70\1\147\1\153\1\155\1\157\1\162\1\164\1\70\4\uffff\1\172\5\uffff\1\u0080\1\u0081\2\uffff\1\u0084\1\uffff\1\64\2\u0086\2\64\3\uffff\3\70\1\uffff\7\70\1\u009c\1\70\1\u009e\1\u009f\1\u00a0\1\70\1\uffff\5\70\1\uffff\1\70\1\u00a9\2\70\1\u00ac\3\70\1\u00b2\1\u00b5\1\u00b6\2\70\1\u00b9\3\70\1\u00bd\7\70\3\uffff\1\u00c5\13\uffff\1\u00c6\21\uffff\1\u0086\3\uffff\3\70\1\u00ca\14\70\1\u00d8\1\uffff\1\70\3\uffff\1\70\1\u00db\5\70\1\u00e3\1\uffff\1\70\1\u00e6\1\uffff\5\70\1\uffff\2\70\2\uffff\2\70\1\uffff\3\70\1\uffff\5\70\1\u00f9\1\u00fa\2\uffff\3\70\1\uffff\1\u00fe\5\70\1\u0105\1\u0106\2\70\1\u0109\1\u010a\1\70\1\uffff\2\70\1\uffff\7\70\1\uffff\2\70\1\uffff\2\70\1\u0119\4\70\1\u011e\1\70\1\u0120\1\u0121\1\u0122\1\u0123\1\u0124\1\u0125\1\u0126\1\u0127\1\u0128\2\uffff\2\70\1\u012b\1\uffff\4\70\1\u0130\1\u0131\2\uffff\1\70\1\u0133\2\uffff\13\70\1\u013f\1\70\1\u0141\1\uffff\1\u0142\3\70\1\uffff\1\u0146\11\uffff\1\70\1\u0148\1\uffff\2\70\1\u014b\1\70\2\uffff\1\70\1\uffff\1\70\1\u014f\3\70\1\u0153\1\u0154\1\u0155\1\u0156\1\70\1\u0158\1\uffff\1\u0159\2\uffff\1\u015a\1\u015b\1\u015c\1\uffff\1\70\1\uffff\2\70\1\uffff\1\u0160\2\70\1\uffff\3\70\4\uffff\1\u0166\5\uffff\2\70\1\u0169\1\uffff\1\70\1\u016b\1\u016c\1\u016d\1\u016e\1\uffff\2\70\1\uffff\1\u0171\4\uffff\1\70\1\u0173\1\uffff\1\70\1\uffff\2\70\1\u0177\1\uffff";
    static final String DFA20_eofS =
        "\u0178\uffff";
    static final String DFA20_minS =
        "\1\0\1\141\1\154\1\151\1\146\1\60\1\141\1\60\1\145\1\143\1\141\1\154\1\146\1\141\1\157\1\141\1\145\1\151\1\162\1\141\1\157\1\141\1\75\2\56\3\75\1\111\4\uffff\1\56\5\uffff\2\60\2\uffff\1\60\1\uffff\1\0\2\56\2\0\3\uffff\1\143\1\144\1\163\1\uffff\1\160\1\141\1\145\1\151\1\165\1\144\1\156\1\60\1\160\3\60\1\144\1\uffff\1\162\1\145\1\155\2\162\1\uffff\1\146\1\60\1\164\1\144\1\60\1\143\2\157\3\60\1\166\1\154\1\60\1\157\1\162\1\145\1\60\1\163\1\165\1\160\1\151\2\164\1\162\3\uffff\1\56\13\uffff\1\60\21\uffff\1\56\3\uffff\2\157\1\147\1\60\1\145\1\162\1\145\1\160\1\156\1\164\1\155\1\145\1\151\2\164\1\145\1\60\1\uffff\1\157\3\uffff\1\151\1\60\1\141\1\156\1\160\2\151\1\60\1\uffff\1\151\1\60\1\uffff\1\164\1\154\1\141\1\142\1\163\1\uffff\1\137\1\171\2\uffff\1\145\1\154\1\uffff\1\154\1\144\1\160\1\uffff\1\164\2\145\1\164\1\150\2\60\2\uffff\1\166\1\162\1\145\1\uffff\1\60\1\145\1\156\1\163\1\164\1\171\2\60\1\146\1\154\2\60\1\162\1\uffff\1\162\1\146\1\uffff\1\154\1\141\1\154\1\141\1\156\1\143\1\165\1\uffff\1\157\1\162\1\uffff\1\157\1\145\1\60\1\164\1\141\1\145\1\157\1\60\1\162\11\60\2\uffff\1\145\1\144\1\60\1\uffff\1\163\1\156\1\144\1\145\2\60\2\uffff\1\151\1\60\2\uffff\1\151\1\164\1\151\1\154\1\162\1\145\1\154\1\147\1\164\1\154\1\156\1\60\1\162\1\60\1\uffff\1\60\1\154\1\164\1\146\1\uffff\1\60\11\uffff\1\137\1\60\1\uffff\1\163\1\141\1\60\1\144\2\uffff\1\156\1\uffff\1\164\1\60\2\145\1\151\4\60\1\164\1\60\1\uffff\1\60\2\uffff\3\60\1\uffff\1\144\1\uffff\1\151\1\154\1\uffff\1\60\1\145\1\163\1\uffff\1\162\1\154\1\157\4\uffff\1\60\5\uffff\1\145\1\157\1\60\1\uffff\1\144\4\60\1\uffff\1\146\1\156\1\uffff\1\60\4\uffff\1\141\1\60\1\uffff\1\165\1\uffff\1\154\1\164\1\60\1\uffff";
    static final String DFA20_maxS =
        "\1\uffff\1\151\1\170\1\156\1\164\1\172\1\141\1\172\1\157\1\163\2\154\1\162\2\157\1\141\1\147\1\151\1\171\1\151\1\157\1\141\1\75\1\76\1\71\1\75\1\76\1\75\1\111\4\uffff\1\71\5\uffff\2\172\2\uffff\1\172\1\uffff\1\uffff\1\170\1\71\2\uffff\3\uffff\1\155\1\156\1\163\1\uffff\1\164\1\141\1\145\1\151\1\165\1\164\1\156\1\172\1\160\3\172\1\154\1\uffff\1\162\1\145\1\155\2\162\1\uffff\1\146\1\172\1\164\1\144\1\172\1\154\2\157\3\172\1\166\1\154\1\172\1\157\1\162\1\145\1\172\1\163\1\165\1\160\1\151\2\164\1\162\3\uffff\1\71\13\uffff\1\172\21\uffff\1\71\3\uffff\2\157\1\147\1\172\1\145\1\162\1\145\1\160\1\162\1\164\1\155\1\145\1\151\2\164\1\145\1\172\1\uffff\1\157\3\uffff\1\151\1\172\1\141\1\156\1\160\1\151\1\165\1\172\1\uffff\1\157\1\172\1\uffff\1\164\1\163\1\141\1\142\1\163\1\uffff\1\137\1\171\2\uffff\1\145\1\154\1\uffff\1\154\1\144\1\160\1\uffff\1\164\2\145\1\164\1\150\2\172\2\uffff\1\166\1\162\1\145\1\uffff\1\172\1\145\1\162\1\163\1\164\1\171\2\172\1\146\1\154\2\172\1\162\1\uffff\1\162\1\146\1\uffff\1\154\1\141\1\154\1\141\1\156\1\143\1\165\1\uffff\1\157\1\162\1\uffff\1\157\1\145\1\172\1\164\1\141\1\145\1\157\1\172\1\162\11\172\2\uffff\1\145\1\144\1\172\1\uffff\1\163\1\156\1\144\1\145\2\172\2\uffff\1\151\1\172\2\uffff\1\151\1\164\1\151\1\154\1\162\1\145\1\154\1\147\1\164\1\154\1\156\1\172\1\162\1\172\1\uffff\1\172\1\154\1\164\1\146\1\uffff\1\172\11\uffff\1\137\1\172\1\uffff\1\163\1\141\1\172\1\144\2\uffff\1\156\1\uffff\1\164\1\172\2\145\1\151\4\172\1\164\1\172\1\uffff\1\172\2\uffff\3\172\1\uffff\1\144\1\uffff\1\151\1\154\1\uffff\1\172\1\145\1\163\1\uffff\1\162\1\154\1\157\4\uffff\1\172\5\uffff\1\145\1\157\1\172\1\uffff\1\144\4\172\1\uffff\1\146\1\156\1\uffff\1\172\4\uffff\1\141\1\172\1\uffff\1\165\1\uffff\1\154\1\164\1\172\1\uffff";
    static final String DFA20_acceptS =
        "\35\uffff\1\112\1\113\1\114\1\115\1\uffff\1\117\1\122\1\123\1\127\1\130\2\uffff\1\133\1\134\1\uffff\1\140\5\uffff\1\146\1\147\1\150\3\uffff\1\140\15\uffff\1\136\5\uffff\1\137\31\uffff\1\66\1\111\1\67\1\uffff\1\144\1\120\1\70\1\121\1\71\1\124\1\72\1\73\1\125\1\74\1\126\1\uffff\1\112\1\113\1\114\1\115\1\116\1\117\1\122\1\123\1\127\1\130\1\131\1\132\1\133\1\134\1\135\1\142\1\141\1\uffff\1\145\1\146\1\147\21\uffff\1\102\1\uffff\1\101\1\103\1\104\10\uffff\1\100\2\uffff\1\76\5\uffff\1\106\2\uffff\1\107\1\110\2\uffff\1\77\3\uffff\1\105\7\uffff\1\143\1\75\3\uffff\1\64\15\uffff\1\61\2\uffff\1\62\7\uffff\1\60\2\uffff\1\57\22\uffff\1\63\1\65\3\uffff\1\50\6\uffff\1\41\1\42\2\uffff\1\54\1\53\16\uffff\1\43\4\uffff\1\47\1\uffff\1\40\1\37\1\44\1\45\1\46\1\51\1\52\1\55\1\56\2\uffff\1\35\4\uffff\1\31\1\32\1\uffff\1\36\13\uffff\1\27\1\uffff\1\33\1\34\3\uffff\1\30\1\uffff\1\22\2\uffff\1\14\3\uffff\1\17\3\uffff\1\23\1\24\1\25\1\26\1\uffff\1\13\1\15\1\16\1\20\1\21\3\uffff\1\12\5\uffff\1\11\2\uffff\1\4\1\uffff\1\5\1\6\1\7\1\10\2\uffff\1\3\1\uffff\1\2\3\uffff\1\1";
    static final String DFA20_specialS =
        "\1\0\54\uffff\1\2\2\uffff\1\1\1\3\u0146\uffff}>";
    static final String[] DFA20_transitionS = {
            "\11\64\2\63\2\64\1\63\22\64\1\63\1\26\1\60\1\62\1\64\1\35\1\64\1\61\1\36\1\37\1\40\1\41\1\42\1\27\1\30\1\43\1\56\11\57\1\44\1\64\1\31\1\32\1\33\1\45\1\46\1\47\11\54\1\50\7\54\1\34\7\54\1\51\1\64\1\52\1\64\1\53\1\64\1\11\1\16\1\15\1\10\1\2\1\12\1\13\1\17\1\4\1\54\1\20\1\21\1\5\1\24\1\14\1\6\1\54\1\1\1\7\1\22\1\3\1\25\1\23\3\54\1\64\1\55\uff83\64",
            "\1\66\3\uffff\1\65\3\uffff\1\67",
            "\1\72\1\74\1\75\7\uffff\1\73\1\uffff\1\71",
            "\1\77\4\uffff\1\76",
            "\1\102\6\uffff\1\101\1\100\4\uffff\1\103\1\104",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\16\70\1\105\13\70",
            "\1\107",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\1\111\1\70\1\110\1\70\1\112\16\70\1\113\6\70",
            "\1\115\11\uffff\1\116",
            "\1\117\12\uffff\1\120\4\uffff\1\121",
            "\1\122\12\uffff\1\123",
            "\1\124",
            "\1\125\7\uffff\1\126\3\uffff\1\127",
            "\1\131\2\uffff\1\132\12\uffff\1\130",
            "\1\133",
            "\1\134",
            "\1\135\1\uffff\1\136",
            "\1\137",
            "\1\140\6\uffff\1\141",
            "\1\142\7\uffff\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\152\1\uffff\12\151\4\uffff\1\150",
            "\1\154\1\uffff\12\152",
            "\1\156",
            "\1\160\1\161",
            "\1\163",
            "\1\165",
            "",
            "",
            "",
            "",
            "\1\152\1\uffff\12\152",
            "",
            "",
            "",
            "",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\174\70\1\uffff\uff83\70",
            "\1\152\1\uffff\12\u0087\76\uffff\1\u0085",
            "\1\152\1\uffff\12\u0087",
            "\12\u0088\1\uffff\2\u0088\1\uffff\ufff2\u0088",
            "\12\u0088\1\uffff\2\u0088\1\uffff\24\u0088\1\uffff\uffdd\u0088",
            "",
            "",
            "",
            "\1\u008c\11\uffff\1\u008b",
            "\1\u008e\11\uffff\1\u008d",
            "\1\u008f",
            "",
            "\1\u0090\3\uffff\1\u0091",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096\4\uffff\1\u0098\12\uffff\1\u0097",
            "\1\u0099",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\7\70\1\u009a\13\70\1\u009b\6\70",
            "\1\u009d",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00a1\7\uffff\1\u00a2",
            "",
            "\1\u00a3",
            "\1\u00a4",
            "\1\u00a5",
            "\1\u00a6",
            "\1\u00a7",
            "",
            "\1\u00a8",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00aa",
            "\1\u00ab",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00ad\10\uffff\1\u00ae",
            "\1\u00af",
            "\1\u00b0",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\5\70\1\u00b1\24\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\4\70\1\u00b3\6\70\1\u00b4\16\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00b7",
            "\1\u00b8",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00ba",
            "\1\u00bb",
            "\1\u00bc",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00be",
            "\1\u00bf",
            "\1\u00c0",
            "\1\u00c1",
            "\1\u00c2",
            "\1\u00c3",
            "\1\u00c4",
            "",
            "",
            "",
            "\1\152\1\uffff\12\151",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\152\1\uffff\12\u0087",
            "",
            "",
            "",
            "\1\u00c7",
            "\1\u00c8",
            "\1\u00c9",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00cb",
            "\1\u00cc",
            "\1\u00cd",
            "\1\u00ce",
            "\1\u00cf\3\uffff\1\u00d0",
            "\1\u00d1",
            "\1\u00d2",
            "\1\u00d3",
            "\1\u00d4",
            "\1\u00d5",
            "\1\u00d6",
            "\1\u00d7",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u00d9",
            "",
            "",
            "",
            "\1\u00da",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00dc",
            "\1\u00dd",
            "\1\u00de",
            "\1\u00df",
            "\1\u00e0\13\uffff\1\u00e1",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\1\u00e2\31\70",
            "",
            "\1\u00e4\5\uffff\1\u00e5",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u00e7",
            "\1\u00e9\6\uffff\1\u00e8",
            "\1\u00ea",
            "\1\u00eb",
            "\1\u00ec",
            "",
            "\1\u00ed",
            "\1\u00ee",
            "",
            "",
            "\1\u00ef",
            "\1\u00f0",
            "",
            "\1\u00f1",
            "\1\u00f2",
            "\1\u00f3",
            "",
            "\1\u00f4",
            "\1\u00f5",
            "\1\u00f6",
            "\1\u00f7",
            "\1\u00f8",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "\1\u00fb",
            "\1\u00fc",
            "\1\u00fd",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u00ff",
            "\1\u0101\3\uffff\1\u0100",
            "\1\u0102",
            "\1\u0103",
            "\1\u0104",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u0107",
            "\1\u0108",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u010b",
            "",
            "\1\u010c",
            "\1\u010d",
            "",
            "\1\u010e",
            "\1\u010f",
            "\1\u0110",
            "\1\u0111",
            "\1\u0112",
            "\1\u0113",
            "\1\u0114",
            "",
            "\1\u0115",
            "\1\u0116",
            "",
            "\1\u0117",
            "\1\u0118",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u011a",
            "\1\u011b",
            "\1\u011c",
            "\1\u011d",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u011f",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "\1\u0129",
            "\1\u012a",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u012c",
            "\1\u012d",
            "\1\u012e",
            "\1\u012f",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "\1\u0132",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "\1\u0134",
            "\1\u0135",
            "\1\u0136",
            "\1\u0137",
            "\1\u0138",
            "\1\u0139",
            "\1\u013a",
            "\1\u013b",
            "\1\u013c",
            "\1\u013d",
            "\1\u013e",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u0140",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u0143",
            "\1\u0144",
            "\1\u0145",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\u0147",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u0149",
            "\1\u014a",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u014c",
            "",
            "",
            "\1\u014d",
            "",
            "\1\u014e",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u0150",
            "\1\u0151",
            "\1\u0152",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u0157",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u015d",
            "",
            "\1\u015e",
            "\1\u015f",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\1\u0161",
            "\1\u0162",
            "",
            "\1\u0163",
            "\1\u0164",
            "\1\u0165",
            "",
            "",
            "",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "",
            "",
            "",
            "\1\u0167",
            "\1\u0168",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u016a",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u016f",
            "\1\u0170",
            "",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "",
            "",
            "",
            "\1\u0172",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            "",
            "\1\u0174",
            "",
            "\1\u0175",
            "\1\u0176",
            "\12\70\7\uffff\32\70\4\uffff\1\70\1\uffff\32\70",
            ""
    };

    static final short[] DFA20_eot = DFA.unpackEncodedString(DFA20_eotS);
    static final short[] DFA20_eof = DFA.unpackEncodedString(DFA20_eofS);
    static final char[] DFA20_min = DFA.unpackEncodedStringToUnsignedChars(DFA20_minS);
    static final char[] DFA20_max = DFA.unpackEncodedStringToUnsignedChars(DFA20_maxS);
    static final short[] DFA20_accept = DFA.unpackEncodedString(DFA20_acceptS);
    static final short[] DFA20_special = DFA.unpackEncodedString(DFA20_specialS);
    static final short[][] DFA20_transition;

    static {
        int numStates = DFA20_transitionS.length;
        DFA20_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA20_transition[i] = DFA.unpackEncodedString(DFA20_transitionS[i]);
        }
    }

    class DFA20 extends DFA {

        public DFA20(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 20;
            this.eot = DFA20_eot;
            this.eof = DFA20_eof;
            this.min = DFA20_min;
            this.max = DFA20_max;
            this.accept = DFA20_accept;
            this.special = DFA20_special;
            this.transition = DFA20_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( Remove_default | Expression | Undefined | External | Inherits | Modifier | Parallel | Scenario | Default | Elapsed | Action | Extend | Factor | Global | Import | Offset | One_of | Record | Sample | Serial | String | Struct | Actor | Cover | Event | Every | False | Float | Range | Until | Bool | Call | Emit | Enum | Fall | Hard | Keep | List | Only | Rise | True | Type | Uint | Unit | Wait | With | And | Def | Int | Mol | Not | Rad | Var | ExclamationMarkEqualsSign | HyphenMinusGreaterThanSign | FullStopFullStop | LessThanSignEqualsSign | EqualsSignEqualsSign | EqualsSignGreaterThanSign | GreaterThanSignEqualsSign | SI | As | Cd | Do | If | In | Is | It | Kg | Of | On | Or | ExclamationMark | PercentSign | LeftParenthesis | RightParenthesis | Asterisk | PlusSign | Comma | HyphenMinus | FullStop | Solidus | Colon | LessThanSign | EqualsSign | GreaterThanSign | QuestionMark | CommercialAt | A | K | LeftSquareBracket | RightSquareBracket | KW__ | M | S | RULE_IDENTIFIER | RULE_UINT | RULE_HEX_UINT | RULE_NEG_INT | RULE_FLOAT | RULE_STRING | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA20_0 = input.LA(1);

                        s = -1;
                        if ( (LA20_0=='r') ) {s = 1;}

                        else if ( (LA20_0=='e') ) {s = 2;}

                        else if ( (LA20_0=='u') ) {s = 3;}

                        else if ( (LA20_0=='i') ) {s = 4;}

                        else if ( (LA20_0=='m') ) {s = 5;}

                        else if ( (LA20_0=='p') ) {s = 6;}

                        else if ( (LA20_0=='s') ) {s = 7;}

                        else if ( (LA20_0=='d') ) {s = 8;}

                        else if ( (LA20_0=='a') ) {s = 9;}

                        else if ( (LA20_0=='f') ) {s = 10;}

                        else if ( (LA20_0=='g') ) {s = 11;}

                        else if ( (LA20_0=='o') ) {s = 12;}

                        else if ( (LA20_0=='c') ) {s = 13;}

                        else if ( (LA20_0=='b') ) {s = 14;}

                        else if ( (LA20_0=='h') ) {s = 15;}

                        else if ( (LA20_0=='k') ) {s = 16;}

                        else if ( (LA20_0=='l') ) {s = 17;}

                        else if ( (LA20_0=='t') ) {s = 18;}

                        else if ( (LA20_0=='w') ) {s = 19;}

                        else if ( (LA20_0=='n') ) {s = 20;}

                        else if ( (LA20_0=='v') ) {s = 21;}

                        else if ( (LA20_0=='!') ) {s = 22;}

                        else if ( (LA20_0=='-') ) {s = 23;}

                        else if ( (LA20_0=='.') ) {s = 24;}

                        else if ( (LA20_0=='<') ) {s = 25;}

                        else if ( (LA20_0=='=') ) {s = 26;}

                        else if ( (LA20_0=='>') ) {s = 27;}

                        else if ( (LA20_0=='S') ) {s = 28;}

                        else if ( (LA20_0=='%') ) {s = 29;}

                        else if ( (LA20_0=='(') ) {s = 30;}

                        else if ( (LA20_0==')') ) {s = 31;}

                        else if ( (LA20_0=='*') ) {s = 32;}

                        else if ( (LA20_0=='+') ) {s = 33;}

                        else if ( (LA20_0==',') ) {s = 34;}

                        else if ( (LA20_0=='/') ) {s = 35;}

                        else if ( (LA20_0==':') ) {s = 36;}

                        else if ( (LA20_0=='?') ) {s = 37;}

                        else if ( (LA20_0=='@') ) {s = 38;}

                        else if ( (LA20_0=='A') ) {s = 39;}

                        else if ( (LA20_0=='K') ) {s = 40;}

                        else if ( (LA20_0=='[') ) {s = 41;}

                        else if ( (LA20_0==']') ) {s = 42;}

                        else if ( (LA20_0=='_') ) {s = 43;}

                        else if ( ((LA20_0>='B' && LA20_0<='J')||(LA20_0>='L' && LA20_0<='R')||(LA20_0>='T' && LA20_0<='Z')||LA20_0=='j'||LA20_0=='q'||(LA20_0>='x' && LA20_0<='z')) ) {s = 44;}

                        else if ( (LA20_0=='|') ) {s = 45;}

                        else if ( (LA20_0=='0') ) {s = 46;}

                        else if ( ((LA20_0>='1' && LA20_0<='9')) ) {s = 47;}

                        else if ( (LA20_0=='\"') ) {s = 48;}

                        else if ( (LA20_0=='\'') ) {s = 49;}

                        else if ( (LA20_0=='#') ) {s = 50;}

                        else if ( ((LA20_0>='\t' && LA20_0<='\n')||LA20_0=='\r'||LA20_0==' ') ) {s = 51;}

                        else if ( ((LA20_0>='\u0000' && LA20_0<='\b')||(LA20_0>='\u000B' && LA20_0<='\f')||(LA20_0>='\u000E' && LA20_0<='\u001F')||LA20_0=='$'||LA20_0=='&'||LA20_0==';'||LA20_0=='\\'||LA20_0=='^'||LA20_0=='`'||LA20_0=='{'||(LA20_0>='}' && LA20_0<='\uFFFF')) ) {s = 52;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA20_48 = input.LA(1);

                        s = -1;
                        if ( ((LA20_48>='\u0000' && LA20_48<='\t')||(LA20_48>='\u000B' && LA20_48<='\f')||(LA20_48>='\u000E' && LA20_48<='\uFFFF')) ) {s = 136;}

                        else s = 52;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA20_45 = input.LA(1);

                        s = -1;
                        if ( ((LA20_45>='\u0000' && LA20_45<='{')||(LA20_45>='}' && LA20_45<='\uFFFF')) ) {s = 56;}

                        else s = 52;

                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA20_49 = input.LA(1);

                        s = -1;
                        if ( ((LA20_49>='\u0000' && LA20_49<='\t')||(LA20_49>='\u000B' && LA20_49<='\f')||(LA20_49>='\u000E' && LA20_49<='!')||(LA20_49>='#' && LA20_49<='\uFFFF')) ) {s = 136;}

                        else s = 52;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 20, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}