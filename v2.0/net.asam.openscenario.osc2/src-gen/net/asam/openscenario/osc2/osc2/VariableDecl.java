/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.VariableDecl#getSampleExp <em>Sample Exp</em>}</li>
 * </ul>
 *
 * @see net.asam.openscenario.osc2.osc2.Osc2Package#getVariableDecl()
 * @model
 * @generated
 */
public interface VariableDecl extends FieldDeclaration
{
  /**
   * Returns the value of the '<em><b>Sample Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sample Exp</em>' containment reference.
   * @see #setSampleExp(SampleExpression)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getVariableDecl_SampleExp()
   * @model containment="true"
   * @generated
   */
  SampleExpression getSampleExp();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.VariableDecl#getSampleExp <em>Sample Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Sample Exp</em>' containment reference.
   * @see #getSampleExp()
   * @generated
   */
  void setSampleExp(SampleExpression value);

} // VariableDecl
