/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Method Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getName <em>Name</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getParamters <em>Paramters</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getImpl <em>Impl</em>}</li>
 * </ul>
 *
 * @see net.asam.openscenario.osc2.osc2.Osc2Package#getMethodDeclaration()
 * @model
 * @generated
 */
public interface MethodDeclaration extends StructActorMemberDecl
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' attribute.
   * @see #setName(String)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getMethodDeclaration_Name()
   * @model
   * @generated
   */
  String getName();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getName <em>Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' attribute.
   * @see #getName()
   * @generated
   */
  void setName(String value);

  /**
   * Returns the value of the '<em><b>Paramters</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Paramters</em>' containment reference.
   * @see #setParamters(ArgumentListSpecification)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getMethodDeclaration_Paramters()
   * @model containment="true"
   * @generated
   */
  ArgumentListSpecification getParamters();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getParamters <em>Paramters</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Paramters</em>' containment reference.
   * @see #getParamters()
   * @generated
   */
  void setParamters(ArgumentListSpecification value);

  /**
   * Returns the value of the '<em><b>Return Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Return Type</em>' containment reference.
   * @see #setReturnType(TypeDeclaration)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getMethodDeclaration_ReturnType()
   * @model containment="true"
   * @generated
   */
  TypeDeclaration getReturnType();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getReturnType <em>Return Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Return Type</em>' containment reference.
   * @see #getReturnType()
   * @generated
   */
  void setReturnType(TypeDeclaration value);

  /**
   * Returns the value of the '<em><b>Impl</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Impl</em>' containment reference.
   * @see #setImpl(MethodImplementation)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getMethodDeclaration_Impl()
   * @model containment="true"
   * @generated
   */
  MethodImplementation getImpl();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.MethodDeclaration#getImpl <em>Impl</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Impl</em>' containment reference.
   * @see #getImpl()
   * @generated
   */
  void setImpl(MethodImplementation value);

} // MethodDeclaration
