/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2.impl;

import net.asam.openscenario.osc2.osc2.EventCondition;
import net.asam.openscenario.osc2.osc2.Osc2Package;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class EventConditionImpl extends MinimalEObjectImpl.Container implements EventCondition
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EventConditionImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Osc2Package.Literals.EVENT_CONDITION;
  }

} //EventConditionImpl
