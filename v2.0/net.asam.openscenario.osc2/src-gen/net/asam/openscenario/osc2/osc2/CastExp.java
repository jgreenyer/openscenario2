/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cast Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.CastExp#getExp <em>Exp</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.CastExp#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see net.asam.openscenario.osc2.osc2.Osc2Package#getCastExp()
 * @model
 * @generated
 */
public interface CastExp extends Expression
{
  /**
   * Returns the value of the '<em><b>Exp</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Exp</em>' containment reference.
   * @see #setExp(Expression)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getCastExp_Exp()
   * @model containment="true"
   * @generated
   */
  Expression getExp();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.CastExp#getExp <em>Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Exp</em>' containment reference.
   * @see #getExp()
   * @generated
   */
  void setExp(Expression value);

  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(TypeDeclaration)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getCastExp_Type()
   * @model containment="true"
   * @generated
   */
  TypeDeclaration getType();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.CastExp#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(TypeDeclaration value);

} // CastExp
