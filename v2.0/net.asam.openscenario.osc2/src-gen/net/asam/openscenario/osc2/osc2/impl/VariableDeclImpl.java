/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2.impl;

import net.asam.openscenario.osc2.osc2.Osc2Package;
import net.asam.openscenario.osc2.osc2.SampleExpression;
import net.asam.openscenario.osc2.osc2.VariableDecl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable Decl</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.VariableDeclImpl#getSampleExp <em>Sample Exp</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VariableDeclImpl extends FieldDeclarationImpl implements VariableDecl
{
  /**
   * The cached value of the '{@link #getSampleExp() <em>Sample Exp</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSampleExp()
   * @generated
   * @ordered
   */
  protected SampleExpression sampleExp;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VariableDeclImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Osc2Package.Literals.VARIABLE_DECL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SampleExpression getSampleExp()
  {
    return sampleExp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSampleExp(SampleExpression newSampleExp, NotificationChain msgs)
  {
    SampleExpression oldSampleExp = sampleExp;
    sampleExp = newSampleExp;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Osc2Package.VARIABLE_DECL__SAMPLE_EXP, oldSampleExp, newSampleExp);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setSampleExp(SampleExpression newSampleExp)
  {
    if (newSampleExp != sampleExp)
    {
      NotificationChain msgs = null;
      if (sampleExp != null)
        msgs = ((InternalEObject)sampleExp).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Osc2Package.VARIABLE_DECL__SAMPLE_EXP, null, msgs);
      if (newSampleExp != null)
        msgs = ((InternalEObject)newSampleExp).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Osc2Package.VARIABLE_DECL__SAMPLE_EXP, null, msgs);
      msgs = basicSetSampleExp(newSampleExp, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Osc2Package.VARIABLE_DECL__SAMPLE_EXP, newSampleExp, newSampleExp));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case Osc2Package.VARIABLE_DECL__SAMPLE_EXP:
        return basicSetSampleExp(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Osc2Package.VARIABLE_DECL__SAMPLE_EXP:
        return getSampleExp();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Osc2Package.VARIABLE_DECL__SAMPLE_EXP:
        setSampleExp((SampleExpression)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.VARIABLE_DECL__SAMPLE_EXP:
        setSampleExp((SampleExpression)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.VARIABLE_DECL__SAMPLE_EXP:
        return sampleExp != null;
    }
    return super.eIsSet(featureID);
  }

} //VariableDeclImpl
