/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aggregate Type Decl</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.AggregateTypeDecl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see net.asam.openscenario.osc2.osc2.Osc2Package#getAggregateTypeDecl()
 * @model
 * @generated
 */
public interface AggregateTypeDecl extends TypeDeclaration
{
  /**
   * Returns the value of the '<em><b>Type</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Type</em>' containment reference.
   * @see #setType(NonAggregateTypeDecl)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getAggregateTypeDecl_Type()
   * @model containment="true"
   * @generated
   */
  NonAggregateTypeDecl getType();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.AggregateTypeDecl#getType <em>Type</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Type</em>' containment reference.
   * @see #getType()
   * @generated
   */
  void setType(NonAggregateTypeDecl value);

} // AggregateTypeDecl
