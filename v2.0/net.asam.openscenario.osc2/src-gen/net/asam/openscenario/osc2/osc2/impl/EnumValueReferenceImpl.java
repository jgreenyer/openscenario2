/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2.impl;

import net.asam.openscenario.osc2.osc2.EnumDeclaration;
import net.asam.openscenario.osc2.osc2.EnumMemberDecl;
import net.asam.openscenario.osc2.osc2.EnumValueReference;
import net.asam.openscenario.osc2.osc2.Osc2Package;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Enum Value Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.EnumValueReferenceImpl#getEnumRef <em>Enum Ref</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.EnumValueReferenceImpl#getMemberRef <em>Member Ref</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnumValueReferenceImpl extends ExpressionImpl implements EnumValueReference
{
  /**
   * The cached value of the '{@link #getEnumRef() <em>Enum Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEnumRef()
   * @generated
   * @ordered
   */
  protected EnumDeclaration enumRef;

  /**
   * The cached value of the '{@link #getMemberRef() <em>Member Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMemberRef()
   * @generated
   * @ordered
   */
  protected EnumMemberDecl memberRef;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected EnumValueReferenceImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Osc2Package.Literals.ENUM_VALUE_REFERENCE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EnumDeclaration getEnumRef()
  {
    if (enumRef != null && enumRef.eIsProxy())
    {
      InternalEObject oldEnumRef = (InternalEObject)enumRef;
      enumRef = (EnumDeclaration)eResolveProxy(oldEnumRef);
      if (enumRef != oldEnumRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, Osc2Package.ENUM_VALUE_REFERENCE__ENUM_REF, oldEnumRef, enumRef));
      }
    }
    return enumRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumDeclaration basicGetEnumRef()
  {
    return enumRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setEnumRef(EnumDeclaration newEnumRef)
  {
    EnumDeclaration oldEnumRef = enumRef;
    enumRef = newEnumRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Osc2Package.ENUM_VALUE_REFERENCE__ENUM_REF, oldEnumRef, enumRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EnumMemberDecl getMemberRef()
  {
    if (memberRef != null && memberRef.eIsProxy())
    {
      InternalEObject oldMemberRef = (InternalEObject)memberRef;
      memberRef = (EnumMemberDecl)eResolveProxy(oldMemberRef);
      if (memberRef != oldMemberRef)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, Osc2Package.ENUM_VALUE_REFERENCE__MEMBER_REF, oldMemberRef, memberRef));
      }
    }
    return memberRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EnumMemberDecl basicGetMemberRef()
  {
    return memberRef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setMemberRef(EnumMemberDecl newMemberRef)
  {
    EnumMemberDecl oldMemberRef = memberRef;
    memberRef = newMemberRef;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Osc2Package.ENUM_VALUE_REFERENCE__MEMBER_REF, oldMemberRef, memberRef));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Osc2Package.ENUM_VALUE_REFERENCE__ENUM_REF:
        if (resolve) return getEnumRef();
        return basicGetEnumRef();
      case Osc2Package.ENUM_VALUE_REFERENCE__MEMBER_REF:
        if (resolve) return getMemberRef();
        return basicGetMemberRef();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Osc2Package.ENUM_VALUE_REFERENCE__ENUM_REF:
        setEnumRef((EnumDeclaration)newValue);
        return;
      case Osc2Package.ENUM_VALUE_REFERENCE__MEMBER_REF:
        setMemberRef((EnumMemberDecl)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.ENUM_VALUE_REFERENCE__ENUM_REF:
        setEnumRef((EnumDeclaration)null);
        return;
      case Osc2Package.ENUM_VALUE_REFERENCE__MEMBER_REF:
        setMemberRef((EnumMemberDecl)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.ENUM_VALUE_REFERENCE__ENUM_REF:
        return enumRef != null;
      case Osc2Package.ENUM_VALUE_REFERENCE__MEMBER_REF:
        return memberRef != null;
    }
    return super.eIsSet(featureID);
  }

} //EnumValueReferenceImpl
