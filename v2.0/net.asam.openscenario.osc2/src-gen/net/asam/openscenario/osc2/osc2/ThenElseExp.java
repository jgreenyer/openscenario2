/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Then Else Exp</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.ThenElseExp#getThen <em>Then</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.ThenElseExp#getSeperator <em>Seperator</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.ThenElseExp#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @see net.asam.openscenario.osc2.osc2.Osc2Package#getThenElseExp()
 * @model
 * @generated
 */
public interface ThenElseExp extends Expression
{
  /**
   * Returns the value of the '<em><b>Then</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Then</em>' containment reference.
   * @see #setThen(Expression)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getThenElseExp_Then()
   * @model containment="true"
   * @generated
   */
  Expression getThen();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.ThenElseExp#getThen <em>Then</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Then</em>' containment reference.
   * @see #getThen()
   * @generated
   */
  void setThen(Expression value);

  /**
   * Returns the value of the '<em><b>Seperator</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Seperator</em>' attribute.
   * @see #setSeperator(String)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getThenElseExp_Seperator()
   * @model
   * @generated
   */
  String getSeperator();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.ThenElseExp#getSeperator <em>Seperator</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Seperator</em>' attribute.
   * @see #getSeperator()
   * @generated
   */
  void setSeperator(String value);

  /**
   * Returns the value of the '<em><b>Else</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Else</em>' containment reference.
   * @see #setElse(Expression)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getThenElseExp_Else()
   * @model containment="true"
   * @generated
   */
  Expression getElse();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.ThenElseExp#getElse <em>Else</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Else</em>' containment reference.
   * @see #getElse()
   * @generated
   */
  void setElse(Expression value);

} // ThenElseExp
