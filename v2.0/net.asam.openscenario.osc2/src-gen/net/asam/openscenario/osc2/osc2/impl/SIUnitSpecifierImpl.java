/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2.impl;

import net.asam.openscenario.osc2.osc2.Osc2Package;
import net.asam.openscenario.osc2.osc2.SIBaseExponentList;
import net.asam.openscenario.osc2.osc2.SIFactor;
import net.asam.openscenario.osc2.osc2.SIOffset;
import net.asam.openscenario.osc2.osc2.SIUnitSpecifier;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SI Unit Specifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.SIUnitSpecifierImpl#getList <em>List</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.SIUnitSpecifierImpl#getFactor <em>Factor</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.SIUnitSpecifierImpl#getOffset <em>Offset</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SIUnitSpecifierImpl extends UnitSpecifierImpl implements SIUnitSpecifier
{
  /**
   * The cached value of the '{@link #getList() <em>List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getList()
   * @generated
   * @ordered
   */
  protected SIBaseExponentList list;

  /**
   * The cached value of the '{@link #getFactor() <em>Factor</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFactor()
   * @generated
   * @ordered
   */
  protected SIFactor factor;

  /**
   * The cached value of the '{@link #getOffset() <em>Offset</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOffset()
   * @generated
   * @ordered
   */
  protected SIOffset offset;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SIUnitSpecifierImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Osc2Package.Literals.SI_UNIT_SPECIFIER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SIBaseExponentList getList()
  {
    return list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetList(SIBaseExponentList newList, NotificationChain msgs)
  {
    SIBaseExponentList oldList = list;
    list = newList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Osc2Package.SI_UNIT_SPECIFIER__LIST, oldList, newList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setList(SIBaseExponentList newList)
  {
    if (newList != list)
    {
      NotificationChain msgs = null;
      if (list != null)
        msgs = ((InternalEObject)list).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Osc2Package.SI_UNIT_SPECIFIER__LIST, null, msgs);
      if (newList != null)
        msgs = ((InternalEObject)newList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Osc2Package.SI_UNIT_SPECIFIER__LIST, null, msgs);
      msgs = basicSetList(newList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Osc2Package.SI_UNIT_SPECIFIER__LIST, newList, newList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SIFactor getFactor()
  {
    return factor;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetFactor(SIFactor newFactor, NotificationChain msgs)
  {
    SIFactor oldFactor = factor;
    factor = newFactor;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Osc2Package.SI_UNIT_SPECIFIER__FACTOR, oldFactor, newFactor);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setFactor(SIFactor newFactor)
  {
    if (newFactor != factor)
    {
      NotificationChain msgs = null;
      if (factor != null)
        msgs = ((InternalEObject)factor).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Osc2Package.SI_UNIT_SPECIFIER__FACTOR, null, msgs);
      if (newFactor != null)
        msgs = ((InternalEObject)newFactor).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Osc2Package.SI_UNIT_SPECIFIER__FACTOR, null, msgs);
      msgs = basicSetFactor(newFactor, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Osc2Package.SI_UNIT_SPECIFIER__FACTOR, newFactor, newFactor));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public SIOffset getOffset()
  {
    return offset;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOffset(SIOffset newOffset, NotificationChain msgs)
  {
    SIOffset oldOffset = offset;
    offset = newOffset;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Osc2Package.SI_UNIT_SPECIFIER__OFFSET, oldOffset, newOffset);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOffset(SIOffset newOffset)
  {
    if (newOffset != offset)
    {
      NotificationChain msgs = null;
      if (offset != null)
        msgs = ((InternalEObject)offset).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - Osc2Package.SI_UNIT_SPECIFIER__OFFSET, null, msgs);
      if (newOffset != null)
        msgs = ((InternalEObject)newOffset).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - Osc2Package.SI_UNIT_SPECIFIER__OFFSET, null, msgs);
      msgs = basicSetOffset(newOffset, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, Osc2Package.SI_UNIT_SPECIFIER__OFFSET, newOffset, newOffset));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case Osc2Package.SI_UNIT_SPECIFIER__LIST:
        return basicSetList(null, msgs);
      case Osc2Package.SI_UNIT_SPECIFIER__FACTOR:
        return basicSetFactor(null, msgs);
      case Osc2Package.SI_UNIT_SPECIFIER__OFFSET:
        return basicSetOffset(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Osc2Package.SI_UNIT_SPECIFIER__LIST:
        return getList();
      case Osc2Package.SI_UNIT_SPECIFIER__FACTOR:
        return getFactor();
      case Osc2Package.SI_UNIT_SPECIFIER__OFFSET:
        return getOffset();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Osc2Package.SI_UNIT_SPECIFIER__LIST:
        setList((SIBaseExponentList)newValue);
        return;
      case Osc2Package.SI_UNIT_SPECIFIER__FACTOR:
        setFactor((SIFactor)newValue);
        return;
      case Osc2Package.SI_UNIT_SPECIFIER__OFFSET:
        setOffset((SIOffset)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.SI_UNIT_SPECIFIER__LIST:
        setList((SIBaseExponentList)null);
        return;
      case Osc2Package.SI_UNIT_SPECIFIER__FACTOR:
        setFactor((SIFactor)null);
        return;
      case Osc2Package.SI_UNIT_SPECIFIER__OFFSET:
        setOffset((SIOffset)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.SI_UNIT_SPECIFIER__LIST:
        return list != null;
      case Osc2Package.SI_UNIT_SPECIFIER__FACTOR:
        return factor != null;
      case Osc2Package.SI_UNIT_SPECIFIER__OFFSET:
        return offset != null;
    }
    return super.eIsSet(featureID);
  }

} //SIUnitSpecifierImpl
