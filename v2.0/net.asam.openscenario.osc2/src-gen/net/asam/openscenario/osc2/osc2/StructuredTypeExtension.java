/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Structured Type Extension</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.StructuredTypeExtension#getRef <em>Ref</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.StructuredTypeExtension#getMembers <em>Members</em>}</li>
 * </ul>
 *
 * @see net.asam.openscenario.osc2.osc2.Osc2Package#getStructuredTypeExtension()
 * @model
 * @generated
 */
public interface StructuredTypeExtension extends TypeExtension
{
  /**
   * Returns the value of the '<em><b>Ref</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ref</em>' containment reference.
   * @see #setRef(ExtentableTypeName)
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getStructuredTypeExtension_Ref()
   * @model containment="true"
   * @generated
   */
  ExtentableTypeName getRef();

  /**
   * Sets the value of the '{@link net.asam.openscenario.osc2.osc2.StructuredTypeExtension#getRef <em>Ref</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ref</em>' containment reference.
   * @see #getRef()
   * @generated
   */
  void setRef(ExtentableTypeName value);

  /**
   * Returns the value of the '<em><b>Members</b></em>' containment reference list.
   * The list contents are of type {@link net.asam.openscenario.osc2.osc2.ExtensionMemberDecl}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Members</em>' containment reference list.
   * @see net.asam.openscenario.osc2.osc2.Osc2Package#getStructuredTypeExtension_Members()
   * @model containment="true"
   * @generated
   */
  EList<ExtensionMemberDecl> getMembers();

} // StructuredTypeExtension
