/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2.impl;

import java.util.Collection;

import net.asam.openscenario.osc2.osc2.ImportStatement;
import net.asam.openscenario.osc2.osc2.Osc2Package;
import net.asam.openscenario.osc2.osc2.OscDeclaration;
import net.asam.openscenario.osc2.osc2.OscFile;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Osc File</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.OscFileImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.OscFileImpl#getOscDecl <em>Osc Decl</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OscFileImpl extends MinimalEObjectImpl.Container implements OscFile
{
  /**
   * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImports()
   * @generated
   * @ordered
   */
  protected EList<ImportStatement> imports;

  /**
   * The cached value of the '{@link #getOscDecl() <em>Osc Decl</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOscDecl()
   * @generated
   * @ordered
   */
  protected EList<OscDeclaration> oscDecl;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected OscFileImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Osc2Package.Literals.OSC_FILE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ImportStatement> getImports()
  {
    if (imports == null)
    {
      imports = new EObjectContainmentEList<ImportStatement>(ImportStatement.class, this, Osc2Package.OSC_FILE__IMPORTS);
    }
    return imports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<OscDeclaration> getOscDecl()
  {
    if (oscDecl == null)
    {
      oscDecl = new EObjectContainmentEList<OscDeclaration>(OscDeclaration.class, this, Osc2Package.OSC_FILE__OSC_DECL);
    }
    return oscDecl;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case Osc2Package.OSC_FILE__IMPORTS:
        return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
      case Osc2Package.OSC_FILE__OSC_DECL:
        return ((InternalEList<?>)getOscDecl()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Osc2Package.OSC_FILE__IMPORTS:
        return getImports();
      case Osc2Package.OSC_FILE__OSC_DECL:
        return getOscDecl();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Osc2Package.OSC_FILE__IMPORTS:
        getImports().clear();
        getImports().addAll((Collection<? extends ImportStatement>)newValue);
        return;
      case Osc2Package.OSC_FILE__OSC_DECL:
        getOscDecl().clear();
        getOscDecl().addAll((Collection<? extends OscDeclaration>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.OSC_FILE__IMPORTS:
        getImports().clear();
        return;
      case Osc2Package.OSC_FILE__OSC_DECL:
        getOscDecl().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.OSC_FILE__IMPORTS:
        return imports != null && !imports.isEmpty();
      case Osc2Package.OSC_FILE__OSC_DECL:
        return oscDecl != null && !oscDecl.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //OscFileImpl
