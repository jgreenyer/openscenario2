/**
 * generated by Xtext 2.28.0
 */
package net.asam.openscenario.osc2.osc2.impl;

import java.util.Collection;

import net.asam.openscenario.osc2.osc2.ArgumentListSpecification;
import net.asam.openscenario.osc2.osc2.ArgumentSpecification;
import net.asam.openscenario.osc2.osc2.Osc2Package;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Argument List Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link net.asam.openscenario.osc2.osc2.impl.ArgumentListSpecificationImpl#getArgumentSpecifications <em>Argument Specifications</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ArgumentListSpecificationImpl extends MinimalEObjectImpl.Container implements ArgumentListSpecification
{
  /**
   * The cached value of the '{@link #getArgumentSpecifications() <em>Argument Specifications</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getArgumentSpecifications()
   * @generated
   * @ordered
   */
  protected EList<ArgumentSpecification> argumentSpecifications;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ArgumentListSpecificationImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return Osc2Package.Literals.ARGUMENT_LIST_SPECIFICATION;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<ArgumentSpecification> getArgumentSpecifications()
  {
    if (argumentSpecifications == null)
    {
      argumentSpecifications = new EObjectContainmentEList<ArgumentSpecification>(ArgumentSpecification.class, this, Osc2Package.ARGUMENT_LIST_SPECIFICATION__ARGUMENT_SPECIFICATIONS);
    }
    return argumentSpecifications;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case Osc2Package.ARGUMENT_LIST_SPECIFICATION__ARGUMENT_SPECIFICATIONS:
        return ((InternalEList<?>)getArgumentSpecifications()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case Osc2Package.ARGUMENT_LIST_SPECIFICATION__ARGUMENT_SPECIFICATIONS:
        return getArgumentSpecifications();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case Osc2Package.ARGUMENT_LIST_SPECIFICATION__ARGUMENT_SPECIFICATIONS:
        getArgumentSpecifications().clear();
        getArgumentSpecifications().addAll((Collection<? extends ArgumentSpecification>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.ARGUMENT_LIST_SPECIFICATION__ARGUMENT_SPECIFICATIONS:
        getArgumentSpecifications().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case Osc2Package.ARGUMENT_LIST_SPECIFICATION__ARGUMENT_SPECIFICATIONS:
        return argumentSpecifications != null && !argumentSpecifications.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //ArgumentListSpecificationImpl
